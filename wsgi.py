"""
WSGI config for rifugiati project

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""

import os
import sys

from django.core.wsgi import get_wsgi_application


path = os.path.dirname(__file__)

sys.path.append(path + "/lib/python2.7/site-packages/mayan/apps")
os.environ.setdefault("BASE_DIR", path + "/lib/python2.7/site-packages/mayan")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings_local")

application = get_wsgi_application()