from __future__ import unicode_literals

from functools import partial

from django.db import models
from django.db.models.signals import post_delete, post_save
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _


@python_2_unicode_compatible
class ActionLogger(models.Model):

    actor = models.CharField(_('Actor'), max_length=255)
    verb = models.CharField(_('Verb'), max_length=255)
    target = models.CharField(_('Target'), max_length=255)
    target_model =  models.CharField(_('Target model'), max_length=255)
    when = models.DateTimeField(_('When'), auto_now_add=True)

    def __str__(self):              # __unicode__ on Python 2
        return "%s: %s %s %s %s" % (self.when, self.actor, self.verb, self.target_model, self.target)

    def __unicode__(self):
        return str(self)


class ActionRegistry():

    _registry = []

    @classmethod
    def _log(self, verb, target_model, sender, instance, **kwargs):
        if kwargs.get('created'):
            verb = 'created'
        from threadlocals.threadlocals import get_current_user
        al = ActionLogger(actor="%s" % get_current_user(), verb=verb, target="%s" % instance, target_model=target_model)
        al.save()
    
    @classmethod
    def register(cls, model):
        if model not in cls._registry:
            cls._registry.append(model)
            post_save.connect(partial(cls._log, 'saved', model._meta.verbose_name.title()), sender=model, weak=False)
            post_delete.connect(partial(cls._log, 'deleted', model._meta.verbose_name.title()), sender=model, weak=False)
