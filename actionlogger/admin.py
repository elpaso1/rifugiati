# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import ActionLogger


@admin.register(ActionLogger)
class ActionLoggerAdmin(admin.ModelAdmin):
    list_display = ('when', 'actor', 'verb', 'target_model', 'target')

