# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-09-05 20:00
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('actionlogger', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='actionlogger',
            name='target_model',
            field=models.CharField(default='void', max_length=50, verbose_name='Target model'),
            preserve_default=False,
        ),
    ]
