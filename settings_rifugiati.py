
from settings_base import *
from django.utils.translation import ugettext_lazy as _

PROJECT_TITLE = 'CSD - Servizio Migranti'

INSTALLED_APPS = INSTALLED_APPS + \
    ('tinymce',
     'imagefit',
     'rifugiati_commons',
     'equipe',
     'comuni_italiani',
     'beneficiari',
     'appartamenti',
     'django_countries',
     'bootstrap_datepicker',
     'actionlogger',
     'django_extensions',
     'localflavor',
    )

# Add request to the metadata template renderer
MIDDLEWARE_CLASSES = MIDDLEWARE_CLASSES + (
    'threadlocals.middleware.ThreadLocalMiddleware',
)

LANGUAGE_CODE = 'it-it'
DOCUMENTS_LANGUAGE = 'ita'

TINYMCE_INCLUDE_JQUERY = False
TINYMCE_DEFAULT_CONFIG = {
    'theme': "advanced",
    'theme_advanced_buttons1' : 'bold,italic,underline,separator,bullist,numlist,separator,formatselect,separator,undo,redo',
    'theme_advanced_buttons2' : '',
    'theme_advanced_buttons3' : '',
    'relative_urls': False,
    'height' : "280",
}

IMAGEFIT_ROOT = "./mayan-edms/"

STRONGHOLD_PUBLIC_URLS = (r'^/rifugiati_commons/imagefit/resize/', )


EMAIL_TO_DEBUG_ADDRESS = 'apasotti@gmail.com'

METADATA_AVAILABLE_PARSERS = [u'metadata.parsers.DateAndTimeParser',
    u'metadata.parsers.DateParser',
    u'metadata.parsers.TimeParser',
    u'rifugiati_commons.metadata_parsers.ItalianDateParser']

METADATA_AVAILABLE_VALIDATORS = [u'metadata.validators.DateAndTimeValidator',
    u'metadata.validators.DateValidator',
    u'metadata.validators.TimeValidator',
    u'rifugiati_commons.metadata_validators.ItalianDateValidator',
    u'rifugiati_commons.metadata_validators.EmailValidator']

OCR_AUTO_OCR = False
