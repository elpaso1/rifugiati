# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging
from collections import OrderedDict

from bootstrap_datepicker.widgets import DatePicker
from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import string_concat
from django.utils.translation import ugettext_lazy as _

from acls.models import AccessControlList
from appartamenti.models import Appartamento
from equipe.models import Equipe
from tags.models import Tag
from tags.permissions import permission_tag_view
from tags.widgets import TagFormWidget

from .models import Beneficiario, Diario

logger = logging.getLogger(__name__)

class BeneficiarioForm(forms.ModelForm):

    class Meta:
        model = Beneficiario
        fields = '__all__'
        widgets = {
            'entry_date' : DatePicker(options={"format": "yyyy-mm-dd", "autoclose": True}),
            'exit_date' : DatePicker(options={"format": "yyyy-mm-dd", "autoclose": True}),
            'birthday' : DatePicker(options={"format": "yyyy-mm-dd", "autoclose": True}),
        }

    def __init__(self,*args,**kwargs):
        super (BeneficiarioForm,self ).__init__(*args,**kwargs) # populates the post
        self.fields['equipe'].queryset = Equipe.equipe_accessible_objects.all()
        self.fields['apartment'].queryset = Appartamento.equipe_accessible_objects.all()
        if kwargs.get('instance') and kwargs.get('instance').pk:
            self.fields['attestato_nominativo'].queryset = kwargs.get('instance').get_documents()
            self.fields['c3'].queryset = kwargs.get('instance').get_documents()
            self.fields['pds'].queryset = kwargs.get('instance').get_documents()
            self.fields['rrpds'].queryset = kwargs.get('instance').get_documents()
        else:
            self.fields = OrderedDict([(k, v) for k, v in self.fields.items() if k not in ('c3', 'pds', 'rrpds')])


    def clean(self):
        """Checks:
            - exit date and in project flag
            - apartment and equipe relationship
        """
        cleaned_data = super(BeneficiarioForm, self).clean()
        equipe = cleaned_data.get("equipe")
        apartment = cleaned_data.get("apartment")
        exit_date = cleaned_data.get("exit_date")
        in_project = cleaned_data.get("in_project")

        if equipe and apartment:
            if apartment.equipe != equipe:
                raise forms.ValidationError(
                    "L'appartamento selezionato non appartiene all'equipe: correggi questa incoerenza scegliendo un appartamento che appartiene all'equipe oppure modificando l'equipe in modo che corrisponda a quello dell'appartamento."
                )

        if exit_date and in_project:
            raise forms.ValidationError( "Una data di uscita dal progetto è stata impostata ma il flag 'In progetto' è ancora attivo: correggi questa incoerenza eliminando la data di uscita oppure deselezionando la checkbox 'In progetto'.")


class DiarioForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        help_text = kwargs.pop('help_text', None)
        queryset = kwargs.pop('queryset', Tag.objects.all())
        user = kwargs.pop('user', None)

        logger.debug('user: %s', user)
        super(DiarioForm, self).__init__(*args, **kwargs)

        queryset = AccessControlList.objects.filter_by_access(
            permission_tag_view, user, queryset=queryset
        )

        self.fields['tags'] = forms.ModelMultipleChoiceField(
            label=_('Tags'), help_text=help_text,
            queryset=queryset, required=False,
            widget=TagFormWidget(
                attrs={'class': 'select2-tags'}, queryset=queryset
            )
        )

    class Meta:
        model = Diario
        fields = ('testo', 'tags')
