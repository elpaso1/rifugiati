# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ugettext
from django_countries.fields import CountryField
from rifugiati_commons.validators import validate_codice_fiscale
from tinymce.models import HTMLField
from django.contrib.auth.models import User
from localflavor.generic.models import IBANField

from appartamenti.models import Appartamento
from equipe.models import Equipe
from extras import CharNullField
from rifugiati_commons.models import RifugiatiAbstractBaseModel, UserEquipeManager

from documents.models import Document

from tags.models import Tag

@python_2_unicode_compatible
class Beneficiario(RifugiatiAbstractBaseModel):
    """Model definition for Beneficiario."""

    GENDER_CHOICES = (
        ('MALE', _('Maschio')),
        ('FEMALE', _('Femmina')),
        ('N/D', _('Sconosciuto/non divulgato')),
    )

    name = models.CharField(verbose_name=_("Nome"), max_length=64, db_index=True)
    family_name = models.CharField(verbose_name=_("Cognome"), max_length=64, db_index=True)
    in_project = models.BooleanField(verbose_name=_("In progetto"), max_length=64)
    vestanet = CharNullField(verbose_name=_("VESTANET"), max_length=9, blank=True, null=True, unique=True, db_index=True)
    photo = models.ImageField(upload_to='uploads/%Y/%m/%d/', verbose_name=_("Fotografia"), blank=True, null=True)
    genere = models.CharField(max_length=6, choices=GENDER_CHOICES, default='N/D')
    alias = models.CharField(verbose_name=_("Alias"), max_length=64, blank=True, null=True)
    birthday = models.DateField(verbose_name=_("Data di nascita"), auto_now=False, auto_now_add=False)
    country = CountryField(verbose_name=_("Nazionalità"), db_index=True)
    lingua_madre = models.CharField(verbose_name=_("Lingua madre"), max_length=128, blank=True, null=True)
    lingua_veicolare = models.CharField(verbose_name=_("Lingua veicolare"), max_length=128, blank=True, null=True)
    status = models.CharField(verbose_name=_("Status"), max_length=128, blank=True, null=True)
    documento_in_corso = models.CharField(verbose_name=_("Documento in corso di validità"), help_text=_("Descrizione e codice alfanumerico del documento di cui è in possesso il beneficiario"), max_length=512, default="XXXXXXXXXXXXX")
    carta_identita =  models.CharField(verbose_name=_("Carta d'identità (numero)"), max_length=64, blank=True, null=True)
    titolo_di_viaggio = models.CharField(verbose_name=_("Titolo di viaggio"), max_length=128, blank=True, null=True)
    iban =  IBANField(verbose_name=_("IBAN"), blank=True, null=True)

    c3 = models.ForeignKey(Document, verbose_name=_("C3"), max_length=64, db_index=True, help_text=_('Primo documento assegnato alla richiesta di asilo. Questo campo viene automaticamente compilato (o sovrascritto) quando un documento con questa categoria viene caricato.'), blank=True, null=True, related_name='c3_document')
    attestato_nominativo = models.ForeignKey(Document, verbose_name=_("Attestato nominativo"), blank=True, null=True, related_name='attestato_nominativo_document')
    pds = models.ForeignKey(Document, verbose_name=_("Permesso di soggiorno in corso di validità"), help_text=_('Permesso di soggiorno valido. Questo campo viene automaticamente compilato (o sovrascritto) quando un documento con questa categoria viene caricato.'), blank=True, null=True, related_name='pds_document')
    rrpds = models.ForeignKey(Document, verbose_name=_("Richiesta di rinnovo del permesso di soggiorno"), help_text=_('Richiesta di rinnovo permesso di soggiorno. Questo campo viene automaticamente compilato (o sovrascritto) quando un documento con questa categoria viene caricato.'), blank=True, null=True, related_name='rrpds_document')
    entry_date = models.DateField(verbose_name=_("Data di ingresso nel progetto"), auto_now_add=False, db_index=True)
    exit_date = models.DateField(verbose_name=_("Data di uscita dal progetto"), auto_now=False, auto_now_add=False, db_index=True, blank=True, null=True)
    motivo_uscita = models.CharField(verbose_name=_("Motivo dell'uscita dal progetto"), blank=True, null=True, max_length=512)
    equipe = models.ForeignKey(Equipe, on_delete=models.CASCADE)
    residency_permit = models.CharField(verbose_name=_("Permesso di soggiorno in corso di validità"), max_length=64, blank=True, null=True)
    home_address = models.CharField(verbose_name=_("Residenza"), max_length=128, help_text=_('Indicare il domicilio o l\'effettiva residenza'))
    apartment = models.ForeignKey(Appartamento, verbose_name=_("Domicilio"), on_delete=models.CASCADE)
    phone = models.CharField(verbose_name=_("Telefono"), max_length=10, blank=True, null=True)
    fiscal_code = models.CharField(verbose_name=_("Codice fiscale"), max_length=16, blank=True, null=True) # No: possibile schema diverso, validators=[validate_codice_fiscale])
    fiscal_code2 = models.CharField(verbose_name=_("Codice fiscale numerico"), max_length=24, blank=True, null=True) # No: possibile schema diverso, validators=[validate_codice_fiscale])
    commissione_territoriale = models.DateField(verbose_name=_("Commissione territoriale"), max_length=64, blank=True, null=True)
    esito_commissione_territoriale = models.CharField(verbose_name=_("Esito commissione territoriale"), max_length=32, blank=True, null=True)
    ricorso_1 = models.CharField(verbose_name=_("Ricorso"), max_length=128, blank=True, null=True, help_text=_("Indicare la data dell'udienza"))
    esito_ricorso_1 = models.CharField(verbose_name=_("Esito ricorso"), max_length=128, blank=True, null=True, help_text=_("Indicare l'esito e la data di notifica"))
    ricorso_2 = models.CharField(verbose_name=_("Appello"), max_length=128, blank=True, null=True, help_text=_("Indicare la data dell'udienza"))
    esito_ricorso_2 = models.CharField(verbose_name=_("Esito appello"), max_length=128, blank=True, null=True, help_text=_("Indicare l'esito e la data di notifica"))
    ricorso_3 = models.CharField(verbose_name=_("Cassazione"), max_length=128, blank=True, null=True, help_text=_("Indicare la data dell'udienza, l'esito e la data di notifica"))
    project_notes = HTMLField(verbose_name=_("Sintesi progetto"), blank=True, null=True) # TORM
    scuola = HTMLField(verbose_name=_("Formazione"), blank=True, null=True, help_text=_('Inserire il curriculum scolastico e formativo')) # TORM
    lavoro = HTMLField(verbose_name=_("Lavoro"), blank=True, null=True, help_text=_('Inserire esperienze lavorative e di volontariato')) # TORM
    sanitaria = HTMLField(verbose_name=_("Situazione sanitaria"), blank=True, null=True, help_text=_("Descrizione della situazione sanitaria"))

    # PAI
    pai_accoglienza = HTMLField(verbose_name=_("accoglienza"), blank=True, null=True, help_text=_("Descrizione accoglienza"))
    pai_situazione_lavorativa_pregressa = HTMLField(verbose_name=_("Situazione socio lavorativa pregressa"), blank=True, null=True, help_text=_("Descrizione situazione socio lavorativa pregressa"))
    pai_iter_richiesta_asilo = HTMLField(verbose_name=_("Iter richiesta d'asilo"), blank=True, null=True, help_text=_("Descrizione iter richiesta d'asilo"))
    pai_salute_benessere_psicofisico = HTMLField(verbose_name=_("Salute benessere psicofisico"), blank=True, null=True, help_text=_("Descrizione salute benessere psicofisico"))
    pai_apprendimento_italiano = HTMLField(verbose_name=_("Apprendimento della lingua italiana"), blank=True, null=True, help_text=_("Descrizione apprendimento della lingua italiana"))
    pai_formazione_scolastica_professionale = HTMLField(verbose_name=_("Formazione scolastica professionale"), blank=True, null=True, help_text=_("Descrizione formazione scolastica professionale"))
    pai_percorso_inserimento_lavorativo = HTMLField(verbose_name=_("Percorso inserimento lavorativo"), blank=True, null=True, help_text=_("Descrizione percorso inserimento lavorativo"))
    pai_partecipazione_al_progetto = HTMLField(verbose_name=_("Partecipazione al progetto"), blank=True, null=True, help_text=_("Descrizione partecipazione al progetto"))
    pai_autonomia_richiesta_rilascio_documenti = HTMLField(verbose_name=_("Autonomia richiesta rilascio documenti"), blank=True, null=True, help_text=_("Descrizione autonomia richiesta rilascio documenti"))
    pai_autonomia_conoscenza_del_territorio = HTMLField(verbose_name=_("Autonomia conoscenza del territorio"), blank=True, null=True, help_text=_("Descrizione autonomia conoscenza del territorio"))
    pai_autonomia_socio_lavorativa = HTMLField(verbose_name=_("Autonomia socio-lavorativa"), blank=True, null=True, help_text=_("Descrizione autonomia socio-lavorativa"))

    # Manager
    objects = models.Manager() # The default manager.


    class Meta:
        """Meta definition for Beneficiario."""
        verbose_name = _('Beneficiario')
        verbose_name_plural = _('Beneficiari')
        ordering = ('family_name', 'name')

    @property
    def apartment_label(self):
        """Return not assigned if null"""
        if self.apartment:
            return self.apartment.metadata_value
        else:
            return self.apartment.APARTMENT_NOT_ASSIGNED


    @property
    def full_name(self):
        return "%s %s" % (self.family_name, self.name)

    def __str__(self):              # __unicode__ on Python 2
        return self.full_name

    def __unicode__(self):
        return str(self.full_name)

    def get_absolute_url(self):
        return reverse('beneficiari:beneficiario_detail', args=(self.pk,))

    def get_diari(self):
        return Diario.objects.filter(beneficiario=self)


@python_2_unicode_compatible
class Diario(models.Model):
    """Model definition for Diario."""

    beneficiario = models.ForeignKey(Beneficiario)
    created = models.DateField(verbose_name=_("Data"), auto_now_add=True)
    utente = models.ForeignKey(User)
    testo = HTMLField(verbose_name=_("Testo"))

    tags = models.ManyToManyField(
        Tag, related_name='tags', verbose_name=_('Tags')
    )


    def __str__(self):
        return "Diario di %s - %s by %s" % (self.beneficiario, self.created, self.utente)

    def __unicode__(self):
        return str(self)

