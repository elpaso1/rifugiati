# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging
from functools import partial
from cStringIO import StringIO
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import Context, Template
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ungettext
from django.views.generic import DetailView, ListView, TemplateView
from django.contrib import messages
from weasyprint import HTML, default_url_fetcher
from django.http import JsonResponse,HttpResponseRedirect
from beneficiari.models import Beneficiario, Diario
from common.views import (SingleObjectCreateView, SingleObjectDeleteView,
                          SingleObjectEditView)
from documents.models import Document
from rifugiati_commons.views import (RifugiatiSingleObjectDetailView,
                                     RifugiatiSingleObjectListView)
from common.generics import ObjectPermissionCheckMixin
from .forms import BeneficiarioForm, DiarioForm
from .permissions import (permission_beneficiario_create,
                          permission_beneficiario_delete,
                          permission_beneficiario_edit,
                          permission_beneficiario_view,
                          )

from collections import OrderedDict
from rest_framework import serializers

from acls.models import AccessControlList
from tags.forms import TagMultipleSelectionForm
from tags.models import Tag

class BeneficiarioList(RifugiatiSingleObjectListView):
    model = Beneficiario
    template_name = 'beneficiari/generic_list.html'
    title = _('Elenco beneficiari')

    def get_queryset(self):
        qs = super(BeneficiarioList, self).get_queryset()
        if self.request.GET.get('q'):
            q = self.request.GET.get('q')
            return qs.filter(Q(name__icontains=q) | Q(family_name__icontains=q) | Q(vestanet__icontains=q) |Q(fiscal_code__icontains=q)  |Q(fiscal_code2__icontains=q) )
        return qs

    def get_extra_context(self):
        return {
            'search_terms': self.request.GET.get('q', ''),
            'title': self.title
        }


class BeneficiarioInProject(BeneficiarioList):
    title = _('Elenco beneficiari in progetto')
    def get_queryset(self):
        qs = super(BeneficiarioInProject, self).get_queryset()
        return qs.filter(in_project=True)

class BeneficiarioOutProject(BeneficiarioList):
    title = _('Elenco beneficiari non in progetto')
    def get_queryset(self):
        qs = super(BeneficiarioOutProject, self).get_queryset()
        return qs.filter(in_project=False)


class BeneficiarioDetail(RifugiatiSingleObjectDetailView):
    fields = '__all__'
    model = Beneficiario
    object_permission = permission_beneficiario_view
    template_name = 'beneficiari/beneficiario_detail.html'

    def get_extra_context(self):
        tags = set()
        for taglist in [d.tags.all() for d in  self.object.diario_set.filter(tags__isnull=False)]:
            for t in taglist:
                tags.add(t)
        return {
            'diari_form': DiarioForm(user=self.request.user),
            'diari_tags': tags,
        }

class BeneficiarioPdfSelect(BeneficiarioDetail):
    title = _('Seleziona i campi da esportare nel PDF')
    template_name = 'beneficiari/beneficiario_pdf_select.html'
    always_print = ('name', 'family_name')

    def get_extra_context(self):
        not_blank = OrderedDict()
        for name, field in self.get_form().fields.items():
            value = getattr(self.get_form().instance, name)
            if value is not '' and value is not None:
                not_blank[name] = field
        col_break = len(not_blank) // 2  if len(not_blank) > 10 else 0
        return {
            'title': self.title,
            'not_blank': not_blank,
            'col_break': col_break,
            'always_print': self.always_print,
        }


class BeneficiarioPdf(BeneficiarioDetail):
    template_name = 'beneficiari/generic_form_pdf.html'
    file_name_schema = "%s Scheda.pdf"

    def post(self, *args, **kwargs):
        self.object = self.get_object()
        content = render_to_string(self.get_template_names(), context=Context(self.get_context_data()))
        if self.request.POST.get('html'):
            return HttpResponse(content)
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="%s"' % (self.file_name_schema % self.object.full_name)


        # Use weasyprint:
        #"""
        html = HTML(string=content, base_url=self.request.build_absolute_uri())
        html.write_pdf(response)
        return response
        #"""
        # Use open office
        file_content = StringIO()
        file_content.write( content.encode('utf8') )
        from converter.classes import ConverterBase
        c = ConverterBase(file_content, 'text/html')
        r = c.to_pdf()
        response.content = b''.join([c for c in r])

        return response

    def get_context_data(self, **kwargs):
        data = super(BeneficiarioPdf, self).get_context_data(**kwargs)
        fields = self.request.POST.getlist('fields')
        expanded_fields = ['project_notes', 'scuola', 'lavoro', 'sanitaria', 'photo', 'pds', 'rrpds']
        # all PAI are expanded
        for f in fields:
            if f.startswith('pai_'):
                expanded_fields.append(f)
        data.update(
            {
                'include_diario': self.request.POST.get('include_diario') == '1',
                'diari': self.object.diario_set.all(),
                'show_fields': fields,
                'skip_fields': ('name', 'family_name'),
                'expanded_fields': expanded_fields,
                'title': self.object.full_name,
                'document_fields': [f.name for f in Beneficiario._meta.fields if f.related_model == Document],
                'form': BeneficiarioForm(instance=self.object),
            }
        )
        return data


class BeneficiarioCreate(SingleObjectCreateView):
    model = Beneficiario
    view_permission = permission_beneficiario_create
    form_class = BeneficiarioForm
    post_action_redirect = reverse_lazy('beneficiari:beneficiario_list')

    def get_extra_context(self):
        return {
            'title': _('Crea Beneficiario'),
        }

class BeneficiarioEdit(SingleObjectEditView):
    model = Beneficiario
    form_class = BeneficiarioForm
    object_permission = permission_beneficiario_edit
    post_action_redirect = reverse_lazy('beneficiari:beneficiario_list')

    def get_extra_context(self):
        return {
            'object': self.get_object(),
            'title': _('Modifica Beneficiario: %s') % self.get_object(),
        }


class BeneficiarioDelete(SingleObjectDeleteView):
    model = Beneficiario
    object_permission = permission_beneficiario_edit
    post_action_redirect = reverse_lazy('beneficiari:beneficiario_list')

    def get_extra_context(self):
        return {
            'object': self.get_object(),
            'title': _('Elimina Beneficiario: %s?') % self.get_object(),
        }

## Diari #####################################################################

class DiarioCheckBeneficiarioPermissionMixin(object):

    def get_instance_extra_data(self):
        return {
            'beneficiario': Beneficiario.objects.get(pk=self.kwargs.get('beneficiario_pk')),
            'utente': self.request.user,
        }

    def get_permission_object(self):
        return self.get_instance_extra_data()['beneficiario']


class BeneficiarioDiarioCreate(DiarioCheckBeneficiarioPermissionMixin, SingleObjectCreateView):
    fields = ('testo',)
    model = Diario
    object_permission = permission_beneficiario_edit

    def post(self,  *args, **kwargs):
        form = self.get_form()
        if self.request.is_ajax():
            try:
                self.form_valid(form)
                storage = messages.get_messages(self.request)
                storage.used = True
                # Add tags
                for tag_id in self.request.POST.getlist('tags'):
                    try:
                        form.instance.tags.add(Tag.objects.get(pk=tag_id))
                    except (ValueError, Tag.DoesNotExist):
                        pass
                return JsonResponse({'result': True, 'diario': render_to_string('beneficiari/diario_row.html', {'object': form.instance }) })
            except Exception as e:
                return JsonResponse({'result': False, 'error': str(e)})
        else:
            return super(BeneficiarioDiarioCreate, self).post(*args, **kwargs)

    def get_extra_context(self):
        return {
            'title': _('Crea pagina di diario di %s') % Beneficiario.objects.get(pk=self.kwargs.get('beneficiario_pk')).full_name,
        }



class BeneficiarioDiarioDelete(DiarioCheckBeneficiarioPermissionMixin, SingleObjectDeleteView):
    model = Diario
    object_permission = permission_beneficiario_edit

    def get_extra_context(self):
        return {
            'title': _('Elimina pagina di diario di %s') % Beneficiario.objects.get(pk=self.kwargs.get('beneficiario_pk')).full_name
        }

class BeneficiarioDiarioEdit(DiarioCheckBeneficiarioPermissionMixin, SingleObjectEditView):
    model = Diario
    fields = ('testo', 'tags')
    object_permission = permission_beneficiario_edit


    def form_valid(self, form):
        # This overrides the original Django form_valid method
        # ABP: and saves the tags m2m

        self.object = form.save(commit=False)

        if hasattr(self, 'get_instance_extra_data'):
            for key, value in self.get_instance_extra_data().items():
                setattr(self.object, key, value)

        if hasattr(self, 'get_save_extra_data'):
            save_extra_data = self.get_save_extra_data()
        else:
            save_extra_data = {}

        context = self.get_context_data()
        object_name = self.get_object_name(context=context)

        try:
            self.object.save(**save_extra_data)
            # ABP: added m2m
            form.save_m2m()
        except Exception as exception:
            messages.error(
                self.request,
                _('%(object)s not updated, error: %(error)s.') % {
                    'object': object_name,
                    'error': exception
                }
            )

            raise exception
        else:
            messages.success(
                self.request,
                _(
                    '%(object)s updated successfully.'
                ) % {'object': object_name}
            )

        return HttpResponseRedirect(self.get_success_url())

    def get_extra_context(self):
        return {
            'title': _('Modifica pagina di diario di %s') % Beneficiario.objects.get(pk=self.kwargs.get('beneficiario_pk')).full_name
        }

