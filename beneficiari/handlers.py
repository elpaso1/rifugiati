from __future__ import absolute_import, unicode_literals
import logging
from cabinets.models import Cabinet
from equipe.literals import *
from metadata.models import MetadataType
from .literals import BENEFICIARIO_PERMISSIONS
from . import permissions as permissions_module
from rifugiati_commons.utils import add_instance_acl_to_equipe
from rifugiati_commons.literals import BENEFICIARIO_METADATA_NAME

logger = logging.getLogger(__name__)


def on_beneficiario_save_handler(sender, **kwargs):
    """Takes care of creating cabinets, 
    assign ACLs for Equipe members,
    fix metadata values when name changes"""
    beneficiario = kwargs['instance']
    logger.debug('instance: %s', beneficiario)

    # Assume a cabinet for the parent does exist
    equipe_cabinet = Cabinet.objects.get(label=beneficiario.equipe.metadata_value)
    
    try:
        apartment_cabinet = Cabinet.objects.get(label=beneficiario.apartment_label)
    except Cabinet.DoesNotExist:
        apartment_cabinet = Cabinet(label=beneficiario.apartment_label, parent=equipe_cabinet)
        apartment_cabinet.save()

    # Check and create a cabinet for the beneficiario, parent is the region
    try:
        cabinet = Cabinet.objects.get(label__endswith=beneficiario.key_value)
        # Ensure that the name still matches
        if beneficiario.metadata_value != cabinet.label:
            cabinet.label = beneficiario.metadata_value
            cabinet.save()
    except Cabinet.DoesNotExist:
        cabinet = Cabinet(label=beneficiario.metadata_value, parent=apartment_cabinet)
        cabinet.save()

    # Fix all metadata for documents
    try:
        md = MetadataType.objects.get(name=BENEFICIARIO_METADATA_NAME)
        for dmd in md.documentmetadata_set.filter(value__endswith=beneficiario.key_value):
            if dmd.value != beneficiario.metadata_value:
                dmd.value = beneficiario.metadata_value
                dmd.save()
    except MetadataType.DoesNotExist:
        pass

    # Add permissions
    add_instance_acl_to_equipe(beneficiario, BENEFICIARIO_PERMISSIONS, permissions_module, 'permission_beneficiario', beneficiario.equipe)
