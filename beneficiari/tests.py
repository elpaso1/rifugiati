# -*- coding: utf-8 -*-
# Create your tests here.
from __future__ import absolute_import

from django.contrib.auth.models import Group, User
from django.core.management import call_command
from django.test import TestCase, override_settings

from beneficiari.literals import BENEFICIARIO_PERMISSIONS
from beneficiari.models import Beneficiario
from rifugiati_commons.utils import get_document_type_name
from documents.models import Document


@override_settings(OCR_AUTO_OCR=False, 
                    CELERY_EAGER_PROPAGATES_EXCEPTIONS=True,
                    CELERY_ALWAYS_EAGER=True,
                    BROKER_BACKEND='memory')                    
class RifugiatiHandlerTestCase(TestCase):
    """Test the Document Handlers"""

    @classmethod
    def setUpClass(cls):
        super(RifugiatiHandlerTestCase, cls).setUpClass()
        call_command('metadata_init')
        # Use the populate command to create some initial documents
        call_command('populate', numero_rifugiati=2, numero_documenti=2, numero_equipe=2, numero_appartamenti=2)
    
   
    def test_acls(self):
        """Check wether ACLs have been set for beneficiario"""
        self.assertGreater(Beneficiario.objects.count(), 0)
        for d in Beneficiario.objects.all():
            self.assertEqual(d.acls.count(), 1)
            # Check that ACL is for equipe members
            self.assertEqual(d.acls.all()[0].role.label, d.equipe.metadata_value)

    def test_equipe_accessible_manager(self):
        """Test custom manager"""
        Beneficiario.equipe_accessible_objects._current_user = None
        user = User.objects.create(username='User 999', password='User 999')
        Beneficiario.equipe_accessible_objects._current_user = user
        self.assertEqual(Beneficiario.equipe_accessible_objects.all().count(), 8)
        user.groups.add(Group.objects.get(name__contains='Test Equipe 0'))
        self.assertEqual(Beneficiario.equipe_accessible_objects.all().count(), 4)
        self.assertEqual(list(Beneficiario.equipe_accessible_objects.all()), list(Beneficiario.objects.filter(equipe__name__contains='Test Equipe 0')))
        # Both equipes
        user.groups.add(Group.objects.get(name__contains='Test Equipe 1'))
        self.assertEqual(Beneficiario.equipe_accessible_objects.all().count(), 8)
        self.assertEqual(list(Beneficiario.equipe_accessible_objects.filter(equipe__name__contains='Test Equipe 0')), list(Beneficiario.objects.filter(equipe__name__contains='Test Equipe 0')))
        self.assertEqual(list(Beneficiario.equipe_accessible_objects.filter(equipe__name__contains='Test Equipe 1')), list(Beneficiario.objects.filter(equipe__name__contains='Test Equipe 1')))
        user.delete()
        Beneficiario.equipe_accessible_objects._current_user = None
   
    def test_get_documents(self):
        """Test retrieve documents and document types"""
        a = Beneficiario.objects.all()[0]
        self.assertEqual(a.get_document_type().label, get_document_type_name(a.get_metadata_name()))
        # Check that metadata matches
        self.assertTrue(a.get_documents().count())
        for d in a.get_documents():
            self.assertEqual(d.metadata_value_of.Beneficiario, a.metadata_value)

    def test_documents_deleted(self):
        """Test that related documents are deleted"""
        a = Beneficiario.objects.all()[0]
        self.assertGreater(a.get_documents().count(), 0)
        document_keys = [d.pk for d in a.get_documents()]
        for k in document_keys:
            self.assertEqual(Document.objects.filter(pk=k).count(), 1)
        a.delete()
        for k in document_keys:
            self.assertEqual(Document.objects.filter(pk=k).count(), 0)
