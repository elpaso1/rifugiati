from __future__ import absolute_import, unicode_literals

from django.utils.translation import ugettext_lazy as _

from permissions import PermissionNamespace

namespace = PermissionNamespace('beneficiari', _('Beneficiari'))

permission_beneficiario_create = namespace.add_permission(
    name='beneficiario_create', label=_('Crea beneficiari')
)
permission_beneficiario_delete = namespace.add_permission(
    name='beneficiario_delete', label=_('Elimina beneficiari')
)
permission_beneficiario_edit = namespace.add_permission(
    name='beneficiario_edit', label=_('Modifica beneficiari')
)
permission_beneficiario_view = namespace.add_permission(
    name='beneficiario_view', label=_('Visualizza beneficiari')
)
