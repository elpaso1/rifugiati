from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from navigation import Menu

menu_beneficiari = Menu(
    icon='fa fa-user', label=_('Beneficiari'), name='beneficiari menu'
)
