# Template tags for equipe
from __future__ import absolute_import, unicode_literals

from django import template
from django.utils.safestring import mark_safe
from django.utils.html import escape
from threadlocals.threadlocals import get_current_user

from beneficiari.models import Beneficiario
from documents.models import Document
from tags.widgets import widget_single_tag

register = template.Library()

@register.simple_tag
def beneficiari_of_user():
    """TODO: filter for user
    FIXME: not used anywhere, remove?"""
    user = get_current_user()
    return ','.join([e.metadata_value for e in Beneficiario.objects.all()])


@register.simple_tag
def document(pk):
    """Return the document from the pk"""
    try:
        return Document.objects.get(pk=pk)
    except Document.DoesNotExist:
        return None


@register.filter
def diario_tags(diario):
    """
    A tag widget that displays the tags for the given diario
    """

    result = ['<div class="tag-container diario-tag-container">']

    tags = diario.tags.all()

    for tag in tags:
        result.append(widget_single_tag(tag))

    result.append('</div>')

    return mark_safe(''.join(result))


def widget_single_tag(tag):
    return mark_safe(
        '''
            <span class="label label-tag" style="background: {}">{}</span>
        '''.format(tag.color, escape(tag.label).replace(' ', '&nbsp;'))
    )
