# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2018-01-18 14:39
from __future__ import unicode_literals

from django.db import migrations
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('beneficiari', '0005_auto_20180108_0913'),
    ]

    operations = [
        migrations.AlterField(
            model_name='beneficiario',
            name='sanitaria',
            field=tinymce.models.HTMLField(blank=True, help_text='Descrizione della situazione sanitaria', null=True, verbose_name='Situazione sanitaria'),
        ),
    ]
