from __future__ import absolute_import, unicode_literals

import copy

from django.utils.translation import ugettext_lazy as _

from acls.links import link_acl_list
from navigation import Link

from .permissions import (
    permission_beneficiario_create,
    permission_beneficiario_delete, permission_beneficiario_edit,
    permission_beneficiario_view
)

# beneficiario links

link_custom_acl_list = copy.copy(link_acl_list)


link_beneficiario_create = Link(
    icon='fa fa-user-plus', permissions=(permission_beneficiario_create,),
    text=_('Crea beneficiario'), view='beneficiari:beneficiario_create'
)
link_beneficiario_delete = Link(
    permissions=(permission_beneficiario_delete,), tags='dangerous',
    text=_('Elimina'), view='beneficiari:beneficiario_delete', args='object.pk'
)
link_beneficiario_edit = Link(
    permissions=(permission_beneficiario_edit,), text=_('Modifica'),
    view='beneficiari:beneficiario_edit', args='object.pk'
)
link_beneficiario_list = Link(
    icon='fa fa-columns', text=_('All'), view='beneficiari:beneficiario_list'
)
link_beneficiario_in_project = Link(
    icon='fa fa-columns', text=_('In progetto'), view='beneficiari:beneficiario_in_project'
)
link_beneficiario_out_project = Link(
    icon='fa fa-columns', text=_('Non in progetto'), view='beneficiari:beneficiario_out_project'
)

link_beneficiario_view = Link(
    permissions=(permission_beneficiario_view,), text=_('Details'),
    view='beneficiari:beneficiario_detail', args='object.pk'
)

link_beneficiario_pdf = Link(
    permissions=(permission_beneficiario_view,), text=_('Pdf Scheda'),
    view='beneficiari:beneficiario_pdf', args='object.pk'
)

link_beneficiario_pdf_select = Link(
    permissions=(permission_beneficiario_view,), text=_('Pdf Scheda'),
    view='beneficiari:beneficiario_pdf_select', args='object.pk'
)


