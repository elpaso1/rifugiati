from django.conf.urls import url
from beneficiari.views import *

urlpatterns = [
    url(r'^$', BeneficiarioList.as_view(), name='beneficiario_list'),
    url(r'^create/$', BeneficiarioCreate.as_view(), name='beneficiario_create'),
    url(r'^in_project/$', BeneficiarioInProject.as_view(), name='beneficiario_in_project'),
    url(r'^out_project/$', BeneficiarioOutProject.as_view(), name='beneficiario_out_project'),
    url(r'^(?P<pk>\d+)/$', BeneficiarioDetail.as_view(), name='beneficiario_detail'),
    url(r'^(?P<pk>\d+)/delete/$', BeneficiarioDelete.as_view(), name='beneficiario_delete'),
    url(r'^(?P<pk>\d+)/edit/$', BeneficiarioEdit.as_view(), name='beneficiario_edit'),
    url(r'^(?P<pk>\d+)/pdf/$', BeneficiarioPdf.as_view(), name='beneficiario_pdf'),
    url(r'^(?P<pk>\d+)/pdf_select/$', BeneficiarioPdfSelect.as_view(), name='beneficiario_pdf_select'),
    url(r'^(?P<beneficiario_pk>\d+)/diario/add/$', BeneficiarioDiarioCreate.as_view(), name='beneficiario_diario_add'),
    url(r'^(?P<beneficiario_pk>\d+)/diario/delete/(?P<pk>\d+)/$', BeneficiarioDiarioDelete.as_view(), name='beneficiario_diario_delete'),
    url(r'^(?P<beneficiario_pk>\d+)/diario/edit/(?P<pk>\d+)/$', BeneficiarioDiarioEdit.as_view(), name='beneficiario_diario_edit'),
]

api_urls = []