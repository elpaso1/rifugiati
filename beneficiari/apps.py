# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.apps import apps
from django.core.urlresolvers import reverse_lazy
from django.db.models.signals import post_save, post_delete
from django.utils.translation import ugettext_lazy as _

from acls import ModelPermission
from acls.permissions import permission_acl_edit, permission_acl_view
from common import (MayanAppConfig, menu_facet, menu_main, menu_multi_item,
                    menu_object, menu_secondary, menu_sidebar)
from metadata import MetadataLookup
from rest_api.classes import APIEndPoint
from navigation import SourceColumn

from .links import (link_custom_acl_list, link_beneficiario_create,
                    link_beneficiario_delete, link_beneficiario_edit, link_beneficiario_list,
                    link_beneficiario_view, link_beneficiario_pdf_select,
                    link_beneficiario_in_project, link_beneficiario_out_project)
from .menus import menu_beneficiari
from .permissions import (permission_beneficiario_delete,
                          permission_beneficiario_edit,
                          permission_beneficiario_view,
                          )

from rifugiati_commons.literals import BENEFICIARIO_METADATA_LOOKUP_FUNCTION_NAME

# Mayan < 2.3
try:
    from commons import menu_front_page
except:
    from common.classes import DashboardWidget


class BeneficiariApp(MayanAppConfig):
    name = 'beneficiari'
    test = True
    verbose_name = _('Beneficiari')

    def ready(self):
        super(BeneficiariApp, self).ready()

        Beneficiario = self.get_model('Beneficiario')
        Diario = self.get_model('Diario')


        APIEndPoint(app=self, version_string='1')

        ModelPermission.register(
            model=Beneficiario, permissions=(
                permission_acl_edit, permission_acl_view,
                permission_beneficiario_delete, permission_beneficiario_edit,
                permission_beneficiario_view
            )
        )

        menu_beneficiari.bind_links(
            links=(
                link_beneficiario_list, link_beneficiario_in_project,
                link_beneficiario_out_project, link_beneficiario_create
            )
        )

        menu_main.bind_links(links=(menu_beneficiari,), position=98)

        menu_object.bind_links(
            links=(
                link_beneficiario_view, link_beneficiario_edit,
                link_custom_acl_list, link_beneficiario_delete,
                link_beneficiario_pdf_select
            ), sources=(Beneficiario,)
        )

        # Mayan < 2,3
        try:
            menu_front_page.bind_links(
                links=( link_beneficiario_list, )
            )
        except:
            DashboardWidget(
                icon='fa fa-user', queryset=Beneficiario.objects.all(),
                label=_('Beneficiari'),
                link=reverse_lazy(
                    'beneficiari:beneficiario_list'
                )
            )

        # Custom handlers for beneficiari sync
        from .handlers import on_beneficiario_save_handler
        post_save.connect(
            receiver=on_beneficiario_save_handler,
            sender=Beneficiario
        )


        # Get all beneficiari
        def get_beneficiari(*args, **kwargs ):
            return','.join([e.metadata_value for e in self.get_model('Beneficiario').equipe_accessible_objects.filter(in_project=True)])


        MetadataLookup(
            description=_('Beneficiari'), name=BENEFICIARIO_METADATA_LOOKUP_FUNCTION_NAME,
            value=get_beneficiari
        )

        SourceColumn(
            source=self.get_model('Beneficiario'), label=_('Equipe'),
            func=lambda context: context['object'].equipe
        )

        SourceColumn(
            source=self.get_model('Beneficiario'), label=_('Appartamento'),
            func=lambda context: context['object'].apartment_label
        )

        from actionlogger.models import ActionRegistry
        ActionRegistry.register(Beneficiario)
