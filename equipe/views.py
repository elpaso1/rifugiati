from django.views.generic import ListView, DetailView, TemplateView
from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _, ungettext

from rifugiati_commons.views import RifugiatiSingleObjectDetailView, RifugiatiSingleObjectListView

from equipe.models import Equipe

from .permissions import permission_equipe_create, permission_equipe_delete, permission_equipe_edit, permission_equipe_view

from common.views import (
    SingleObjectCreateView, SingleObjectDeleteView, SingleObjectEditView
    
)
class EquipeList(RifugiatiSingleObjectListView):
    model = Equipe

class EquipeDetail(RifugiatiSingleObjectDetailView):
    model = Equipe
    object_permission = permission_equipe_view
    template_name = 'equipe/equipe_detail.html'

    def get_extra_context(self):
        return {
            'beneficiari_in_project': self.get_object().beneficiario_set.filter(in_project=True),
            'beneficiari_out_project': self.get_object().beneficiario_set.filter(in_project=False),
        }


class EquipeCreate(SingleObjectCreateView):
    fields = ('name', 'description', 'region')
    model = Equipe
    view_permission = permission_equipe_create
    post_action_redirect = reverse_lazy('equipe:equipe_list')

    def get_extra_context(self):
        return {
            'title': _('Crea equipe'),
        }

class EquipeEdit(SingleObjectEditView):
    fields = ('name', 'description', 'region')
    model = Equipe
    object_permission = permission_equipe_edit
    post_action_redirect = reverse_lazy('equipe:equipe_list')

    def get_extra_context(self):
        return {
            'object': self.get_object(),
            'title': _('Modifica equipe: %s') % self.get_object(),
        }


class EquipeDelete(SingleObjectDeleteView):
    model = Equipe
    object_permission = permission_equipe_delete
    post_action_redirect = reverse_lazy('equipe:equipe_list')

    def get_extra_context(self):
        return {
            'object': self.get_object(),
            'title': _('Elimina equipe: %s?') % self.get_object(),
            'relations': self.get_object().beneficiario_set.all(),
        }

