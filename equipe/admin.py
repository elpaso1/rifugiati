from __future__ import unicode_literals

from django.contrib import admin

from .models import Equipe



@admin.register(Equipe)
class EquipeAdmin(admin.ModelAdmin):
    list_display = ('name', 'region')
