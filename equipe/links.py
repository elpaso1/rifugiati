from __future__ import absolute_import, unicode_literals

import copy

from django.utils.translation import ugettext_lazy as _

from acls.links import link_acl_list
from documents.permissions import permission_document_view
from navigation import Link

from .compat import MAYAN_VERSION
from .permissions import (
    permission_equipe_create,
    permission_equipe_delete, permission_equipe_edit,
    permission_equipe_view
)

# equipe links

link_custom_acl_list = copy.copy(link_acl_list)


link_equipe_create = Link(
    icon='fa fa-plus', permissions=(permission_equipe_create,),
    text=_('Crea equipe'), view='equipe:equipe_create'
)
link_equipe_delete = Link(
    permissions=(permission_equipe_delete,), tags='dangerous',
    text=_('Elimina'), view='equipe:equipe_delete', args='object.pk'
)
link_equipe_edit = Link(
    permissions=(permission_equipe_edit,), text=_('Modifica'),
    view='equipe:equipe_edit', args='object.pk'
)
if MAYAN_VERSION[0:2] <= (2, 1):
    link_equipe_list = Link(
        icon='fa fa-columns', text=_('Equipe'), view='equipe:equipe_list'
    )
else:
    link_equipe_list = Link(
        icon='fa fa-columns', text=_('All'), view='equipe:equipe_list'
    )
link_equipe_view = Link(
    permissions=(permission_equipe_view,), text=_('Details'),
    view='equipe:equipe_view', args='object.pk'
)
