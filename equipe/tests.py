from __future__ import absolute_import

from comuni_italiani.models import Regione
from django.contrib.auth.models import Group, User
from django.core.management import call_command
from django.test import TestCase, override_settings

from equipe.models import Equipe
from permissions.models import Role
from rifugiati_commons.management.commands.populate import make_test_document
from rifugiati_commons.utils import get_document_type_name


@override_settings(OCR_AUTO_OCR=False, 
                    CELERY_EAGER_PROPAGATES_EXCEPTIONS=True,
                    CELERY_ALWAYS_EAGER=True,
                    BROKER_BACKEND='memory')                    
class EquipeTestCase(TestCase):
    """Test the Equipe class and handlers"""

    @classmethod
    def setUpClass(cls):
        super(EquipeTestCase, cls).setUpClass()
        call_command('metadata_init')
        Regione.objects.create(name='Regione 1')
        Regione.objects.create(name='Regione 2')
        cls.equipe1 = Equipe.objects.create(name='Equipe 1', region=Regione.objects.all()[0])
        cls.equipe2 = Equipe.objects.create(name='Equipe 2', region=Regione.objects.all()[1])
        cls.user1 = User.objects.create(username='User 1', password='User 1')
        cls.user2 = User.objects.create(username='User 2', password='User 2')
        Group.objects.get(name=cls.equipe1.metadata_value).user_set.add(cls.user1)
        Group.objects.get(name=cls.equipe2.metadata_value).user_set.add(cls.user2)

    @classmethod
    def tearDownClass(cls):
        Regione.objects.all().delete()
        Equipe.objects.all().delete()
        User.objects.all().delete()
        Group.objects.all().delete()        

    def test_metadata_value(self):
        """Test metadata values"""
        self.assertEqual(self.equipe1.metadata_value, '%s [%s-%s]' % (self.equipe1.name, self.equipe1.mod_prefix(), self.equipe1.pk))
        self.assertEqual(self.equipe2.metadata_value, '%s [%s-%s]' % (self.equipe2.name, self.equipe2.mod_prefix(), self.equipe2.pk))

    def test_group_created(self):
        """Test a group exists with the equipe metadata value"""
        self.assertEqual(len(Group.objects.filter(name=self.equipe1.metadata_value)), 1)
        self.assertEqual(len(Group.objects.filter(name=self.equipe2.metadata_value)), 1)

    def test_roles_created(self):
        """Test a role exists with the equipe metadata value"""
        self.equipe1.save()
        self.assertEqual(len(list(Role.objects.filter(label=self.equipe1.metadata_value))), 1)
        self.assertEqual(len(list(Role.objects.filter(label=self.equipe2.metadata_value))), 1)

    def test_get_equipes_for_user(self):
        """Test get users"""
        self.assertEqual([e for e in Equipe.get_equipes_for_user(self.user1)], [self.equipe1])
        self.assertEqual([e for e in Equipe.get_equipes_for_user(self.user2)], [self.equipe2])
        # Test that all items are returned for equipe-less user
        user = User.objects.create(username='User 999', password='User 999')
        equipes = [e for e in Equipe.get_equipes_for_user(user)]
        self.assertTrue(self.equipe1 in equipes)
        self.assertTrue(self.equipe2 in equipes)
        user.delete()

    def test_get_equipe_ids_for_user(self):
        """Test get users"""
        self.assertEqual([e for e in Equipe.get_equipe_ids_for_user(self.user1)], [self.equipe1.pk])
        self.assertEqual([e for e in Equipe.get_equipe_ids_for_user(self.user2)], [self.equipe2.pk])
        # Test that all items are returned for equipe-less user
        user = User.objects.create(username='User 999', password='User 999')
        equipes = [e for e in Equipe.get_equipe_ids_for_user(user)]
        self.assertTrue(self.equipe1.pk in equipes)
        self.assertTrue(self.equipe2.pk in equipes)
        user.delete()

    def test_equipe_accessible_manager(self):
        """Test custom manager"""
        Equipe.equipe_accessible_objects._current_user = None
        user = User.objects.create(username='User 999', password='User 999')
        Equipe.equipe_accessible_objects._current_user = user
        self.assertEqual(Equipe.equipe_accessible_objects.all().count(), 2)
        Equipe.equipe_accessible_objects._current_user = self.user1
        self.assertEqual(Equipe.equipe_accessible_objects.all().count(), 1)
        self.assertEqual(Equipe.equipe_accessible_objects.all()[0], self.equipe1)
        Equipe.equipe_accessible_objects._current_user = self.user2
        self.assertEqual(Equipe.equipe_accessible_objects.all().count(), 1)
        self.assertEqual(Equipe.equipe_accessible_objects.all()[0], self.equipe2)
        user.delete()
        Equipe.equipe_accessible_objects._current_user = None
        

    def test_rename_equipe(self):
        """Test that renaming equipe also renames roles and groups"""
        self.equipe1.name = 'Equipe 1 renamed'
        self.equipe1.save()
        self.assertEqual(self.equipe1.metadata_value, '%s [%s-%s]' % (self.equipe1.name, self.equipe1.mod_prefix(), self.equipe1.pk))
        self.assertEqual(len(Group.objects.filter(name=self.equipe1.metadata_value)), 1)
        self.assertEqual(len(Group.objects.filter(name=self.equipe2.metadata_value)), 1)
        self.assertEqual(len(Role.objects.filter(label=self.equipe1.metadata_value)), 1)
        self.assertEqual(len(Role.objects.filter(label=self.equipe2.metadata_value)), 1)
        self.assertEqual(list(Equipe.get_equipes_for_user(self.user1)), [self.equipe1])
        self.assertEqual(list(Equipe.get_equipes_for_user(self.user2)), [self.equipe2])

    def test_delete_equipe(self):
        """"Test that on equipe delete also groups and roles are deleted"""
        equipe3 = Equipe.objects.create(name='Equipe 3', region=Regione.objects.all()[0])
        self.assertEqual(len(Group.objects.filter(name=equipe3.metadata_value)), 1)
        self.assertEqual(len(list(Role.objects.filter(label=equipe3.metadata_value))), 1)
        

    def test_get_from_metadata(self):
        """Test that an object can be retrieved by metadata value"""
        equipe = Equipe.objects.all()[0]
        self.assertEqual(equipe, Equipe.get_from_metadata(equipe.metadata_value))

    def test_acls(self):
        """Check wether ACLs have been set for equipe"""
        self.assertGreater(Equipe.objects.count(), 0)
        for d in Equipe.objects.all():
            self.assertEqual(d.acls.count(), 1)
            # Check that ACL is for equipe members
            self.assertEqual(d.acls.all()[0].role.label, d.metadata_value)
            self.assertIsNotNone(Role.objects.get(label__endswith=d.metadata_value).permissions.get(name='beneficiario_create'))
       
    def test_get_documents(self):
        """Test retrieve documents and document types"""
        a = Equipe.objects.all()[0]
        self.assertEqual(a.get_document_type().label, get_document_type_name(a.get_metadata_name()))
        make_test_document(a.get_metadata_name(), a.metadata_value, 2)
        # Check that metadata matches
        self.assertTrue(a.get_documents().count())
        for d in a.get_documents():
            self.assertEqual(d.metadata_value_of.Equipe, a.metadata_value)
