# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import re

from comuni_italiani.models import Regione
from django.contrib.auth.models import Group
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from tinymce.models import HTMLField

from rifugiati_commons.models import RifugiatiAbstractBaseModel


@python_2_unicode_compatible
class Equipe(RifugiatiAbstractBaseModel):

    equipe_filter = 'pk__in'

    @classmethod
    def get_equipes_for_user(cls, user):
        """Return equipes filtered by user group, default to all"""
        regexp = re.compile(r'\[.{3}-(\d+)\]')
        try:
            equipe_keys = [int(regexp.findall(g.name)[0]) for g in user.groups.filter(name__contains='[%s-' % Equipe.mod_prefix())]
        except IndexError:
            return cls.objects.all()
        if not len(equipe_keys):
            return cls.objects.all()
        return cls.objects.filter(pk__in=equipe_keys)

    @classmethod
    def get_equipe_ids_for_user(cls, user):
        """Return equipes ids filtered by user group, default to all"""
        return cls.get_equipes_for_user(user).values_list('pk', flat=True)

    name = models.CharField(verbose_name=_("Nome"), max_length=50, unique=True, db_index=True)
    description = HTMLField(verbose_name=_("Descrizione"))
    region = models.ForeignKey(Regione, verbose_name=_('Regione'))

    class Meta:
        ordering = ["-name"]

    def __str__(self):              # __unicode__ on Python 2
        return self.name

    def __unicode__(self):
        return str(self)

    def get_absolute_url(self):
        return reverse('equipe:equipe_view', args=(self.pk,))

    @property
    def users(self):
        """Return all the users that share this equipe group (and role, if it's all in sync)"""
        try:
            group = Group.objects.get(name__endswith=self.key_value)
            return group.user_set.all()
        except Group.DoesNotExist:
            return []
