from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from navigation import Menu

from .compat import MAYAN_VERSION


if MAYAN_VERSION[0:2] <= (2, 1):
    menu_equipe = Menu(name='equipe menu')
else:
    menu_equipe = Menu(
        icon='fa fa-users', label=_('Equipe'), name='equipe menu'
    )
