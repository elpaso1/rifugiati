# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-09-13 09:35
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import tinymce.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('comuni_italiani', '0002_comune_label'),
    ]

    operations = [
        migrations.CreateModel(
            name='Equipe',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(db_index=True, max_length=50, unique=True, verbose_name='Name')),
                ('description', tinymce.models.HTMLField(verbose_name='Description')),
                ('region', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='comuni_italiani.Regione')),
            ],
            options={
                'ordering': ['-name'],
            },
        ),
    ]
