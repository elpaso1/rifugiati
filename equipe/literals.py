from __future__ import unicode_literals

# NOTE: delete is not here! Do not let delete itself! 
EQUIPE_PERMISSIONS = ('view',)
CABINET_PERMISSIONS = ('view', )
BENEFICIARIO_PERMISSIONS = ('create', )
TAG_PERMISSIONS = ('view', 'attach', 'remove')