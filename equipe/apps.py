from __future__ import unicode_literals

from django.conf import settings
from django.apps import apps
from django.core.urlresolvers import reverse_lazy
from django.db.models.signals import post_delete, post_save
from django.utils.translation import ugettext_lazy as _
from acls import ModelPermission
from acls.permissions import permission_acl_edit, permission_acl_view
from common import (MayanAppConfig, menu_facet, menu_main, menu_multi_item,
                    menu_object, menu_secondary, menu_sidebar)
from mayan.celery import app
from metadata import MetadataLookup
from rest_api.classes import APIEndPoint

from .compat import MAYAN_VERSION
from .links import (link_custom_acl_list, link_equipe_create,
                    link_equipe_delete, link_equipe_edit, link_equipe_list,
                    link_equipe_view)
from .menus import menu_equipe
from .permissions import (permission_equipe_delete, permission_equipe_edit,
                          permission_equipe_view)


from rifugiati_commons.literals import EQUIPE_METADATA_LOOKUP_FUNCTION_NAME

# Mayan < 2.3
try:
    from commons import menu_front_page
except:
    from common.classes import DashboardWidget



class EquipeApp(MayanAppConfig):
    name = 'equipe'
    test = True
    verbose_name = _('Equipes')

    def ready(self):
        super(EquipeApp, self).ready()

        equipe = self.get_model('equipe')

        APIEndPoint(app=self, version_string='1')

        ModelPermission.register(
            model=equipe, permissions=(
                permission_acl_edit, permission_acl_view,
                permission_equipe_delete, permission_equipe_edit,
                permission_equipe_view
            )
        )

        if MAYAN_VERSION[0:2] <= (2, 1):
            menu_main.bind_links(
                links=(link_equipe_list,),
            )
            menu_secondary.bind_links(
                links=(link_equipe_create,), sources=(
                    equipe, 'equipe:equipe_list'
                )
            )
        else:
            menu_equipe.bind_links(
                links=(
                    link_equipe_list, link_equipe_create
                )
            )

            menu_main.bind_links(links=(menu_equipe,), position=98)

        menu_object.bind_links(
            links=(
                link_equipe_view, link_equipe_edit,
                link_custom_acl_list, link_equipe_delete
            ), sources=(equipe,)
        )

        # Mayan < 2,3
        try:
            menu_front_page.bind_links(
                links=( link_equipe_list, )
            )
        except:
            DashboardWidget(
                icon='fa fa-users', queryset=self.get_model('Equipe').objects.all(),
                label=_('Equipes'),
                link=reverse_lazy(
                    'equipe:equipe_list'
                )
            )

        # Custom handlers for equipe sync
        from .handlers import on_equipe_post_save_handler, on_equipe_post_delete_handler
        post_save.connect(
            receiver=on_equipe_post_save_handler,
            sender=self.get_model('Equipe')
        )

        post_delete.connect(
            receiver=on_equipe_post_delete_handler,
            sender=self.get_model('Equipe')
        )

        # Get all equipes
        def get_equipe(*args, **kwargs ):
            # If user is in an equipe role, filter by it
            return','.join([e.metadata_value for e in self.get_model('Equipe').equipe_accessible_objects.all()])

        MetadataLookup(
            description=_('Equipe'), name=EQUIPE_METADATA_LOOKUP_FUNCTION_NAME,
            value=get_equipe
        )

        from actionlogger.models import ActionRegistry
        ActionRegistry.register(self.get_model('Equipe'))

