# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import logging

from django.contrib.contenttypes.models import ContentType
from acls.models import AccessControlList
from permissions.models import Role
from cabinets.models import Cabinet
from documents.models import DocumentType
from documents.permissions import permission_document_create
from metadata.models import MetadataType
from rifugiati_commons.literals import (EQUIPE_METADATA_NAME,
                                        RIFUGIATI_MANAGED_METADATA_NAMES)
from rifugiati_commons.utils import (add_instance_acl_to_equipe, delete_role_group,
                                     get_document_type_name, sync_role_group)

from . import permissions as permissions_module
from cabinets import permissions as cabinet_permissions_module
from tags import permissions as tag_permissions_module
from beneficiari import permissions as beneficiario_permissions_module
from .literals import EQUIPE_PERMISSIONS, CABINET_PERMISSIONS, BENEFICIARIO_PERMISSIONS, TAG_PERMISSIONS

logger = logging.getLogger(__name__)


def on_equipe_post_save_handler(sender, **kwargs):
    """Takes care of creating cabinets, roles and groups 
    synced with equipe names, also creates ACLs to create
    selected document types"""
    equipe = kwargs['instance']
    logger.debug('instance: %s', equipe)
    created = kwargs['created']

    # Name of the key
    key_value = equipe.key_value
    # Name for role and group
    metadata_value = equipe.metadata_value

    # First check and create a top level region cabinet
    try:
        region_cabinet = Cabinet.objects.get(label=equipe.region.name)
    except Cabinet.DoesNotExist:
        region_cabinet = Cabinet(label=equipe.region.name)
        region_cabinet.save()

    # Check and create a cabinet for the equipe, parent is the region
    try:
        cabinet = Cabinet.objects.get(label__endswith=key_value)
        # Ensure the name matches
        if cabinet.label != metadata_value:
            cabinet.label = metadata_value
            cabinet.save()
    except Cabinet.DoesNotExist:
        cabinet = Cabinet(label=metadata_value, parent=region_cabinet)
        cabinet.save()

    # Create role and group for the equipe
    sync_role_group(key_value, metadata_value)

    # Sync all metadata for documents
    try:
        md = MetadataType.objects.get(name=EQUIPE_METADATA_NAME)
        for dmd in md.documentmetadata_set.filter(value__endswith=key_value):
            if dmd.value != metadata_value:
                dmd.value = metadata_value
                dmd.save()
    except MetadataType.DoesNotExist:
        pass

    # Add permissions for equipe
    add_instance_acl_to_equipe(equipe, EQUIPE_PERMISSIONS, permissions_module, 'permission_equipe', equipe)

    role = Role.objects.get(label__endswith=key_value)

    # Add permissions to view and use tags
    for perm in TAG_PERMISSIONS:
        role.permissions.add(getattr(tag_permissions_module, 'permission_tag_%s' % 
                perm).stored_permission)

    # Add permission to view cabinets to the equipe
    # ABP: 06/10/2017 Disabled: now equipes do not have access to the cabinets
    # add_instance_acl_to_equipe(region_cabinet, CABINET_PERMISSIONS, cabinet_permissions_module, 'permission_cabinet', equipe)
    
    # Add permission to add beneficiari for the equipe
    add_instance_acl_to_equipe(equipe, BENEFICIARIO_PERMISSIONS, beneficiario_permissions_module, 'permission_beneficiario', equipe)

    # Add permissions for document types to create new documents    
    for dt_name in RIFUGIATI_MANAGED_METADATA_NAMES:
        try:
            dt = DocumentType.objects.get(label=get_document_type_name(dt_name))
            if not len(AccessControlList.objects.filter(object_id=dt.pk, content_type_id=ContentType.objects.get_for_model(DocumentType).pk, role=role)):
                acl = AccessControlList.objects.create(
                    content_object=dt, role=role
                )
                # Add permissions
                acl.permissions.add(permission_document_create.stored_permission)
        
                dt.acls.add(acl)
                logger.debug('ACLs set for %s and Equipe %s' % (dt, equipe))    
            else:
                logger.debug('ACLs already set for %s and Equipe %s' % (dt, equipe))    

        except (DocumentType.DoesNotExist, Role.DoesNotExist):
            pass


def on_equipe_post_delete_handler(sender, **kwargs):
    """Delete all cabinets and synced roles and groups"""
    equipe = kwargs['instance']
    logger.debug('instance: %s', equipe)

      # Name of the key
    key_value = equipe.key_value
    delete_role_group(key_value)
    Cabinet.objects.filter(label__contains=key_value).delete()
