from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from equipe.views import EquipeList, EquipeDetail, EquipeCreate, EquipeDelete, EquipeEdit

urlpatterns = [
    url(r'^$', EquipeList.as_view(), name='equipe_list'),
    url(r'^(?P<pk>\d+)/$', EquipeDetail.as_view(), name='equipe_view'),
    url(r'^create/$', EquipeCreate.as_view(), name='equipe_create'),
    url(r'^(?P<pk>\d+)/delete/$', EquipeDelete.as_view(), name='equipe_delete'),
    url(r'^(?P<pk>\d+)/edit/$', EquipeEdit.as_view(), name='equipe_edit'),
]

api_urls = []