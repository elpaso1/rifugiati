from __future__ import absolute_import, unicode_literals

from django.utils.translation import ugettext_lazy as _

from permissions import PermissionNamespace

namespace = PermissionNamespace('equipes', _('Equipes'))

permission_equipe_create = namespace.add_permission(
    name='equipe_create', label=_('Crea equipe')
)
permission_equipe_delete = namespace.add_permission(
    name='equipe_delete', label=_('Elimina equipe')
)
permission_equipe_edit = namespace.add_permission(
    name='equipe_edit', label=_('Modifica equipe')
)
permission_equipe_view = namespace.add_permission(
    name='equipe_view', label=_('View equipe')
)
