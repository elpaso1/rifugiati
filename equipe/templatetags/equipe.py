# Template tags for equipe
from __future__ import absolute_import

from django import template
from threadlocals.threadlocals import get_current_user
from equipe.models import Equipe

register = template.Library()

@register.simple_tag
def equipe_of_user():
    """TODO: filter for user"""
    user = get_current_user()
    return ','.join([e.metadata_value for e in Equipe.objects.all()])
