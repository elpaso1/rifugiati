from __future__ import unicode_literals

import re
from metadata.validators import MetadataValidator
from dateutil.parser import parserinfo
from dateutil.parser import parse
from django.core.exceptions import ValidationError

class ItalianDateValidator(MetadataValidator):
    """Localized italian date validator for metadata"""
    def execute(self, input_data):
        return parse(input_data, parserinfo=parserinfo(dayfirst=True)).date().isoformat()


class EmailValidator(MetadataValidator):
    """Email validator for metadata"""
    def execute(self, input_data):
        if not re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', input_data):
            raise ValidationError("Inserire un indirizzo email valido")
        return input_data


