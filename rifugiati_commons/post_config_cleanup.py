from __future__ import unicode_literals
from django.conf import settings

def cleanup():
    """Remove menus and widgets"""
    # Patch menus
    from common import (
        MayanAppConfig, menu_facet, menu_main, menu_object, menu_secondary,
        menu_setup, menu_tools, menu_multi_item
    )

    from navigation.classes import Menu, Link
    menu_main = Menu.get('main menu')

    # Remove cabinets menu
    HIDE_CABINETS_MENU = getattr(settings, 'HIDE_CABINETS_MENU', False)
    if HIDE_CABINETS_MENU:
        menu_main.bound_links[None] = [l for l in menu_main.bound_links[None] if isinstance(l, Link)
                                    or (isinstance(l, Menu) and l.name != 'cabinets menu')]

    # Remove indexing and checkouts
    excluded_views = ('checkouts:checkout_list', 'indexing:index_list')
    # excluded_views = ('checkouts:checkout_list',)
    menu_main.bound_links[None] = [l for l in menu_main.bound_links[None] if isinstance(l, Menu)
                                    or (isinstance(l, Link) and l.view not in excluded_views)]

    # Remove dashboard widgets
    #[u'/checkouts/list/',
    #  u'/statistics/new-document-pages-per-month/view/',
    #  u'/statistics/new-documents-per-month/view/',
    #  u'/documents/list/',
    #  u'/documents/type/list/',
    #  u'/documents/list/deleted/']
    #
    from common.classes import DashboardWidget
    excluded_widgets = (
        '/checkouts/list/',
        #'/statistics/new-document-pages-per-month/view/', 
        '/documents/type/list/'
    )
    
    DashboardWidget._registry = [
        w for w in DashboardWidget._registry if "%s" % w.link not in excluded_widgets]

    # Multi item menu
    # [u'documents:document_multiple_clear_transformations',
    # u'documents:document_multiple_trash',
    # u'documents:document_multiple_download_form',
    # u'documents:document_multiple_update_page_count',
    # u'documents:document_multiple_document_type_edit',
    # u'mailer:send_multiple_document',
    # u'mailer:send_multiple_document_link',
    # u'metadata:metadata_multiple_add',
    # u'metadata:metadata_multiple_edit',
    # u'metadata:metadata_multiple_remove',
    # u'ocr:document_submit_multiple',
    # u'tags:multiple_documents_tag_attach',
    # u'tags:multiple_documents_selection_tag_remove']
    # Remove other menus
    excluded_multi_item_views = (
        'documents:document_multiple_clear_transformations',        
        'documents:document_multiple_update_page_count',
        'documents:document_multiple_update_page_count',
        'documents:document_multiple_document_type_edit',
        'ocr:document_submit_multiple',
    )

    if HIDE_CABINETS_MENU:
        excluded_multi_item_views += (
            'cabinets:document_cabinet_list',
            'cabinets:document_cabinet_remove',
            'cabinets:cabinet_add_document',
            'cabinets:cabinet_add_multiple_documents',
            'cabinets:multiple_document_cabinet_remove',
        )


    from documents.models import Document
    menu_multi_item.bound_links[Document] = [l for l in menu_multi_item.bound_links[Document] if isinstance(l, Menu)
                                    or (isinstance(l, Link) and l.view not in excluded_multi_item_views)]

    # Path Tags menu item title
    from tags.menus import menu_tags
    menu_tags.label = 'Etichette'
                            