
from django.conf.urls import url, include

api_urls = []

urlpatterns = [
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^imagefit/', include('imagefit.urls')),
]
