from __future__ import absolute_import, unicode_literals

import logging

from django.contrib.auth.models import Group

from appartamenti.models import *
from beneficiari.models import *
from documents import permissions as permissions_module
from equipe.models import *
from mailer import permissions as mailer_permissions_module
from metadata import permissions as metadata_permissions_module
from rifugiati_commons.literals import (MANAGED_DOCUMENT_PERMISSIONS,
                                        MANAGED_MAILER_PERMISSIONS,
                                        MANAGED_METADATA_PERMISSIONS,
                                        MANAGED_TAGS_PERMISSIONS
                                        )
from rifugiati_commons.utils import (add_instance_acl_to_equipe, document_is_managed_type,
                                     get_equipe_from_metadata)
from tags import permissions as tags_permissions_module

from .literals import *

logger = logging.getLogger(__name__)


def on_document_metadata_post_save_handler(sender, **kwargs):
    """Set ACLs and permissions for the document"""
    dm = kwargs['instance']
    logger.debug('DocumentMetadata instance: %s', dm)

    document = dm.document
    equipe = get_equipe_from_metadata(document)

    if equipe is None:
        logger.warning('Equipe could not be found: %s', document)
        return

    # Add document permissions
    add_instance_acl_to_equipe(document, MANAGED_DOCUMENT_PERMISSIONS, permissions_module, 'permission_document', equipe)
    # Add mailer permissions
    add_instance_acl_to_equipe(document, MANAGED_MAILER_PERMISSIONS, mailer_permissions_module, 'permission_mailing', equipe)
    # Add metadata permissions
    add_instance_acl_to_equipe(document, MANAGED_METADATA_PERMISSIONS, metadata_permissions_module, 'permission_metadata_document', equipe)
    # Add tag permissions
    add_instance_acl_to_equipe(document, MANAGED_TAGS_PERMISSIONS, tags_permissions_module, 'permission_tag', equipe)
