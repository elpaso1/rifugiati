from __future__ import unicode_literals
from datetime import timedelta
from django.apps import apps
from django.conf import settings
from django.db.models.signals import post_save, post_delete
from django.utils.translation import ugettext_lazy as _
from kombu import Exchange, Queue

from acls import ModelPermission
from acls.permissions import permission_acl_edit, permission_acl_view
from common import MayanAppConfig
from mayan.celery import app
from metadata import MetadataLookup
from rest_api.classes import APIEndPoint

from .queues import *  # NOQA

# TODO: needed?
#from rifugiati_commons.literals import CATEGORY_METADATA_LOOKUP_FUNCTION_NAME, CATEGORY_METADATA_LOOKUP_VALUES, CATEGORY_METADATA_NAME

try:
    CHECK_CLASSIFY_PERIOD_INTERVAL = settings.CHECK_CLASSIFY_PERIOD_INTERVAL
except AttributeError:
    CHECK_CLASSIFY_PERIOD_INTERVAL = 60


class RifugiatiCommonsApp(MayanAppConfig):
    name = 'rifugiati_commons'
    test = True
    verbose_name = _('Rifugiati Commons')

    def ready(self):
        super(RifugiatiCommonsApp, self).ready()

        APIEndPoint(app=self, version_string='1')

        # TODO: needed?
        """
        # Get all equipes
        def get_categorie(*args, **kwargs ):
            return ','.join(CATEGORY_METADATA_LOOKUP_VALUES)

        MetadataLookup(
            description=_(CATEGORY_METADATA_NAME), name=CATEGORY_METADATA_LOOKUP_FUNCTION_NAME,
            value=get_categorie
        )
        """

        from .post_config_cleanup import cleanup
        cleanup()

        # Custom handlers for sync
        from .handlers import on_document_metadata_post_save_handler
        DocumentMetadata = apps.get_model(
            app_label='metadata', model_name='DocumentMetadata')

        post_save.connect(
            receiver=on_document_metadata_post_save_handler,
            sender=DocumentMetadata
        )

        app.conf.CELERYBEAT_SCHEDULE.update(
            {
                'task_equipe_document_classify': {
                    'task': 'rifugiati_commons.tasks.task_equipe_document_classify',
                    'schedule': timedelta(
                        seconds=CHECK_CLASSIFY_PERIOD_INTERVAL
                    ),
                },
                'task_beneficiario_document_classify': {
                    'task': 'rifugiati_commons.tasks.task_beneficiario_document_classify',
                    'schedule': timedelta(
                        seconds=CHECK_CLASSIFY_PERIOD_INTERVAL
                    ),
                },
                'task_appartamento_document_classify': {
                    'task': 'rifugiati_commons.tasks.task_appartamento_document_classify',
                    'schedule': timedelta(
                        seconds=CHECK_CLASSIFY_PERIOD_INTERVAL
                    ),
                },
                'task_document_expiry_notification': {
                    'task': 'rifugiati_commons.tasks.task_document_expiry_notification',
                    'schedule': timedelta(
                        days=1
                    ),
                },
            }
        )

        app.conf.CELERY_QUEUES.extend(
            (
                Queue(
                    'equipe_document_classify', Exchange(
                        'equipe_document_classify'),
                    routing_key='equipe_document_classify'
                ),
                Queue(
                    'beneficiario_document_classify', Exchange(
                        'beneficiario_document_classify'),
                    routing_key='beneficiario_document_classify'
                ),
                Queue(
                    'appartamento_document_classify', Exchange(
                        'appartamento_document_classify'),
                    routing_key='appartamento_document_classify'
                ),
                Queue(
                    'task_document_expiry_notification', Exchange(
                        'document_expiry_notification'),
                    routing_key='document_expiry_notification'
                ),
            )
        )

        app.conf.CELERY_ROUTES.update(
            {
                'rifugiati_commons.tasks.task_equipe_document_classify': {
                    'queue': 'equipe_document_classify'
                },
                'rifugiati_commons.tasks.task_beneficiario_document_classify': {
                    'queue': 'beneficiario_document_classify'
                },
                'rifugiati_commons.tasks.task_appartamento_document_classify': {
                    'queue': 'appartamento_document_classify'
                },
                'rifugiati_commons.tasks.task_document_expiry_notification': {
                    'queue': 'document_expiry_notification'
                },
            }
        )

        from actionlogger.models import ActionRegistry
        from documents.models import Document
        from metadata.models import DocumentMetadata
        ActionRegistry.register(Document)
        ActionRegistry.register(DocumentMetadata)

        from metadata.parsers import MetadataParser
        from metadata.validators import MetadataValidator
        from . metadata_parsers import ItalianDateParser
        from . metadata_validators import ItalianDateValidator
        MetadataParser.register(ItalianDateParser)
        MetadataValidator.register(ItalianDateValidator)
        from smart_settings import Setting
        setting = Setting._registry['METADATA_AVAILABLE_VALIDATORS']
        setting.default = MetadataValidator.get_import_paths()
        setting = Setting._registry['METADATA_AVAILABLE_PARSERS']
        setting.default = MetadataParser.get_import_paths()
