# -*- coding: utf-8 -*-

""" Common utility functions for the rifugiati classes"""

import logging

from django.conf import settings
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ImproperlyConfigured

from acls.models import AccessControlList
from metadata.models import DocumentMetadata
from permissions.models import Role

from .literals import (APPARTAMENTO_METADATA_NAME, BENEFICIARIO_METADATA_NAME,
                       EQUIPE_METADATA_NAME, RIFUGIATI_MANAGED_METADATA_NAMES)

logger = logging.getLogger(__name__)


def sync_role_group(key_value, metadata_value):
    """Create a role and group, and test account if DEBUG"""

    # Create a group and a role
    try:
        group = Group.objects.get(name__endswith=key_value)
        # Ensure the name matches
        if group.name != metadata_value:
            group.name = metadata_value
            group.save()
    except Group.DoesNotExist:
        group = Group(name=metadata_value)
        group.save()

    try:
        role = Role.objects.get(label__endswith=key_value)
        # Ensure the name matches
        if role.label != metadata_value:
            role.label = metadata_value
            role.save()
    except Role.DoesNotExist:
        role = Role(label=metadata_value)
        role.save()

    # Add the group to the role
    if group not in role.groups.all():
        role.groups.add(group)

    # Create a test user if debug is on
    if settings.DEBUG:
        from django.contrib.auth.models import User
        try:
            user = User.objects.get(username=metadata_value)
        except User.DoesNotExist:
            user = User.objects.create_user(username=metadata_value,
                                            email='test@email.com',
                                            password=metadata_value)
            user.save()

        # Add to group
        group.user_set.add(user)
        logger.debug(
            u'DEBUG=True: un utente di test %s è stato aggiunto.' % metadata_value)


def delete_role_group(key_value):
    """Delete roles and groups"""
    Role.objects.filter(label__endswith=key_value).delete()
    Group.objects.filter(name__endswith=key_value).delete()


def managed_document_types_labels():
    """Return the labels of the managed metadata types"""
    return [get_document_type_name(l) for l in RIFUGIATI_MANAGED_METADATA_NAMES]


def document_is_managed_type(document):
    """Check wether document type is managed by the application"""
    return document.document_type.label in managed_document_types_labels()


def get_equipe_from_metadata_value(metadata_label, metadata_value):
    """Try to retrieve the Equipe from metadata"""

    from appartamenti.models import Appartamento
    from beneficiari.models import Beneficiario
    from equipe.models import Equipe

    if metadata_label not in RIFUGIATI_MANAGED_METADATA_NAMES:
        return None

    if metadata_label == EQUIPE_METADATA_NAME:
        try:
            return Equipe.get_from_metadata(metadata_value)
        except Equipe.DoesNotExist:
            logger.warning(
                "Equipe could not be found from metatada value: %s" % metadata_value)
            return None

    # Is it an Appartamento document?
    if metadata_label == APPARTAMENTO_METADATA_NAME:
        try:
            return Appartamento.get_from_metadata(metadata_value).equipe
        except Appartamento.DoesNotExist:
            logger.warning(
                "Appartamento could not be found from metatada value: %s" % metadata_value)
            return None

    # Is it an Beneficiario document?
    if metadata_label == BENEFICIARIO_METADATA_NAME:
        try:
            return Beneficiario.get_from_metadata(metadata_value).equipe
        except Beneficiario.DoesNotExist:
            logger.warning(
                "Beneficiario could not be found from metatada value: %s" % metadata_value)
            return None

    logger.warning("Metadata %s is not managed (value=%s" %
                   (metadata_label, metadata_value))
    return None



def get_appartamento_from_metadata_value(metadata_label, metadata_value):
    """Try to retrieve the Appartamento from metadata"""

    from appartamenti.models import Appartamento
    from beneficiari.models import Beneficiario

    if metadata_label not in RIFUGIATI_MANAGED_METADATA_NAMES:
        return None

    # Is it an Appartamento document?
    if metadata_label == APPARTAMENTO_METADATA_NAME:
        try:
            return Appartamento.get_from_metadata(metadata_value)
        except Appartamento.DoesNotExist:
            logger.warning(
                "Appartamento could not be found from metatada value: %s" % metadata_value)
            return None

    # Is it an Beneficiario document?
    if metadata_label == BENEFICIARIO_METADATA_NAME:
        try:
            return Beneficiario.get_from_metadata(metadata_value).apartment
        except Beneficiario.DoesNotExist:
            logger.warning(
                "Beneficiario could not be found from metatada value: %s" % metadata_value)
            return None

    return None


def get_equipe_from_metadata(document):
    """Try to retrieve the equipe from document metadata"""
    if not document_is_managed_type(document):
        logger.warning("Document type%s is not managed" %
                       document.document_type.label)

    # loop through all metadata
    for md in document.metadata.all():
        if not md.value:
            logger.debug("Metadata value is not set for %s" %
                         md.metadata_type.label)
        else:
            equipe = get_equipe_from_metadata_value(
                md.metadata_type.label, md.value)
            if equipe is not None:
                return equipe

    return None

def get_appartamento_from_metadata(document):
    """Try to retrieve the Appartamento from document metadata"""
    if not document_is_managed_type(document):
        logger.warning("Document type%s is not managed" %
                       document.document_type.label)

    # loop through all metadata
    for md in document.metadata.all():
        if not md.value:
            logger.debug("Metadata value is not set for %s" %
                         md.metadata_type.label)
        else:
            apartment = get_appartamento_from_metadata_value(
                md.metadata_type.label, md.value)
            if apartment is not None:
                return apartment

    return None


def get_document_type_name(object_name):
    """Return name of the document type from the object name"""
    return 'Documento %s' % object_name


def add_instance_acl_to_equipe(instance, permissions, permissions_module, permission_prefix, equipe):
    """Add ACL to the instance for the role that matches the Equipe key. 
    Create is added as permission instead of ACL"""

    # Get equipe role
    try:
        role = Role.objects.get(label__endswith=equipe.key_value)
    except Role.DoesNotExist: # retry after saving equipe (that should recreate the role)
        equipe.save()
        role = Role.objects.get(label__endswith=equipe.key_value)

    if 'create' in permissions:
        role.permissions.add(getattr(permissions_module, '%s_create' % 
                permission_prefix).stored_permission)

    if instance is not None and permissions != ('create', ):
        try:
            acl = AccessControlList.objects.get(object_id=instance.pk, 
            content_type_id=ContentType.objects.get_for_model(instance).pk, 
            role=role)
        except AccessControlList.DoesNotExist:
            # Create ACL for document and equipe role
            acl = AccessControlList.objects.create(
                content_object=instance, role=role
            )

        # Add permissions
        for perm in permissions:
            if perm != 'create':
                acl.permissions.add(getattr(permissions_module, '%s_%s' % (
                    permission_prefix, perm)).stored_permission)

        instance.acls.add(acl)
        
    logger.debug('ACLs set for %s and Equipe %s' % (instance, equipe))
