from __future__ import absolute_import, unicode_literals

from django.utils.translation import ugettext_lazy as _

from mayan_task_manager.classes import CeleryQueue

queue_equipe_document_classify = CeleryQueue(
    name='equipe_document_classify', label=_('Equipe: classificazione documenti')
)
queue_equipe_document_classify.add_task_type(
    name='rifugiati_commons.tasks.task_equipe_document_classify',
    label=_('Classifica tutti i documenti equipe')
)

queue_beneficiario_document_classify = CeleryQueue(
    name='beneficiario_document_classify', label=_('Beneficiari: classificazione documenti')
)
queue_beneficiario_document_classify.add_task_type(
    name='rifugiati_commons.tasks.task_beneficiario_document_classify',
    label=_('Classifica tutti i documenti beneficiario')
)

queue_appartamento_document_classify = CeleryQueue(
    name='appartamento_document_classify', label=_('Appartamenti: classificazione documenti')
)
queue_appartamento_document_classify.add_task_type(
    name='rifugiati_commons.tasks.task_appartamento_document_classify',
    label=_('Classifica tutti i documenti appartamento')
)

queue_document_expiry_notification = CeleryQueue(
    name='document_expiry_notification', label=_('Notifiche via email per documenti con metadato Avviso di scadenza')
)

queue_document_expiry_notification.add_task_type(
    name='rifugiati_commons.tasks.task_document_expiry_notification',
    label=_('Invia notifiche via email alla data indicata nel metadato Avviso di scadenza')
)
