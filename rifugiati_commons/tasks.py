# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import logging
import time

from django.apps import apps
from django.conf import settings
from django.core.mail import send_mail

from cabinets.models import Cabinet
from mayan.celery import app
from metadata.models import MetadataType
from rifugiati_commons.literals import (APPARTAMENTO_METADATA_NAME,
                                        AVVISO_DI_SCADENZA_METADATA_NAME,
                                        AVVISO_DI_SCADENZA_EMAIL_METADATA_NAME,
                                        BENEFICIARIO_METADATA_NAME,
                                        EQUIPE_METADATA_NAME,
                                        CATEGORY_METADATA_NAME)

from beneficiari.models import Beneficiario
from equipe.models import Equipe
from appartamenti.models import Appartamento
from .utils import get_equipe_from_metadata

logger = logging.getLogger(__name__)


def add_to_cabinet(metadata_name):
    """Add the document to the appropriate cabinet"""
    Document = apps.get_model(app_label='documents', model_name='Document')

    logger.info('Start documents in cabinets %s' % metadata_name)

    try:
        md = MetadataType.objects.get(name=metadata_name)
        for dmd in md.documentmetadata_set.all():
            try:
                cabinet = Cabinet.objects.get(label=dmd.value)
                cabinet.documents.add(dmd.document)
                logger.debug("Document %s added to cabinet %s" % (dmd.document, cabinet))

            except Cabinet.DoesNotExist:
                logger.critical("Cabinet not found for metadata value %s" % dmd.value)
    except MetadataType.DoesNotExist:
        logger.critical("Metatata type %s does not exists" % metadata_name)

    logger.info('Finished documents in cabinets %s' % metadata_name)


@app.task(ignore_result=True)
def task_equipe_document_classify():
    """Put in cabinets for all documents have an equipe metadata"""
    add_to_cabinet(EQUIPE_METADATA_NAME)


@app.task(ignore_result=True)
def task_beneficiario_document_classify():
    """Put in cabinets for all documents have a beneficiario metadata
    and set special fields on the beneficiario"""
    add_to_cabinet(BENEFICIARIO_METADATA_NAME)

    mdt = MetadataType.objects.get(label=CATEGORY_METADATA_NAME)
    for dm in mdt.documentmetadata_set.all():
        document = dm.document
        try:
            beneficiario = Beneficiario.get_from_metadata(dm.document.metadata.filter(metadata_type__label=BENEFICIARIO_METADATA_NAME)[0].value)
            if dm.value.startswith('C3'):
                beneficiario.c3 = dm.document
                beneficiario.save()
            elif dm.value.startswith('Permesso di soggiorno in corso'):
                beneficiario.pds = dm.document
                beneficiario.save()
            elif dm.value.startswith('Richiesta di rinnovo'):
                beneficiario.rrpds = dm.document
                beneficiario.save()
        except:
            pass



@app.task(ignore_result=True)
def task_appartamento_document_classify():
    """Put in cabinets for all documents have a appartamento metadata"""
    add_to_cabinet(APPARTAMENTO_METADATA_NAME)


@app.task(ignore_result=True)
def task_document_expiry_notification():
    """Send email notifications for documents with metadata starting with 'Avviso di scadenza'"""
    mt = MetadataType.objects.get(label=AVVISO_DI_SCADENZA_METADATA_NAME)
    mt_email = MetadataType.objects.get(label=AVVISO_DI_SCADENZA_EMAIL_METADATA_NAME)
    if mt:
       for dm in mt.documentmetadata_set.filter(value=time.strftime('%Y-%m-%d')):
            document = dm.document
            equipe = get_equipe_from_metadata(document)
            if equipe is not None:
                if settings.DEBUG:
                    emails = [getattr(settings, 'EMAIL_TO_DEBUG_ADDRESS', 'apasotti@gmail.com')]
                else:
                    try:
                        # Get email from metadata
                        emails = [document.metadata.filter(metadata_type=mt_email)[0].value]
                    except IndexError:
                        # Get email from equipe
                        emails = [u.email for u in equipe.users]
                if emails:
                    send_mail(
                        'Avviso di scadenza per documento {0}'.format(document),
                        'Questo è un messaggio automatico: non rispondere a questo messaggio!\n\n'
                        'Chi ha inserito il documento {0} ha impostato un avviso per oggi, controlla se '
                        'il documento è in scadenza e deve essere rinnovato oppure se è richiesta qualche altra azione.'
                        '\nPuoi visualizzare la scheda del documento seguento il link:\n{1}{2}'.format( document, getattr(settings, 'PROJECT_WEBSITE', 'http://localhost:8000'), document.get_absolute_url()),
                        getattr(settings, 'DEFAULT_FROM_EMAIL', 'no-reply@localhost.localdomain'),
                        emails,
                        fail_silently=False,
                    )
            else:
                logger.critical("Document %s has no %s metadata value" % (document, BENEFICIARIO_METADATA_NAME))

