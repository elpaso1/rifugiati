# -*- coding: utf-8 -*-
from __future__ import unicode_literals

EQUIPE_METADATA_NAME = 'Equipe'
BENEFICIARIO_METADATA_NAME = 'Beneficiario'
APPARTAMENTO_METADATA_NAME = 'Appartamento'
AVVISO_DI_SCADENZA_METADATA_NAME = 'Avviso di scadenza (invia una email alla data)'
AVVISO_DI_SCADENZA_EMAIL_METADATA_NAME = 'Email per l\'avviso di scadenza'

# Map module name to metadata name
APP_METADATA_MAP = {
    'equipe': EQUIPE_METADATA_NAME,
    'appartamenti': APPARTAMENTO_METADATA_NAME,
    'beneficiari': BENEFICIARIO_METADATA_NAME,
}

EQUIPE_METADATA_LOOKUP_FUNCTION_NAME = 'equipe'
APPARTAMENTO_METADATA_LOOKUP_FUNCTION_NAME = 'appartamenti'
BENEFICIARIO_METADATA_LOOKUP_FUNCTION_NAME = 'beneficiari'

# The following metadata have a hierarchy that starts with Equipe and are modeled by Rifugiati app
RIFUGIATI_MANAGED_METADATA_NAMES = (EQUIPE_METADATA_NAME, BENEFICIARIO_METADATA_NAME, APPARTAMENTO_METADATA_NAME)


CATEGORY_METADATA_NAME = 'Categoria'
CATEGORY_METADATA_LOOKUP_VALUES = ('Altro', 'C3 (primo documento di richiesta di asilo)', 'Permesso di soggiorno in corso di validità', 'Richiesta di rinnovo del permesso di soggiorno')

MANAGED_DOCUMENT_PERMISSIONS = ('delete', 'download', 'edit', 'new_version', 'print', 'properties_edit', 'restore', 'trash', 'view', 'version_revert')
MANAGED_MAILER_PERMISSIONS = ('link', 'send_document')
MANAGED_METADATA_PERMISSIONS = ('edit', 'add', 'remove', 'view')
MANAGED_TAGS_PERMISSIONS = ('view', 'attach', 'remove')

# Do not translate!
APARTMENT_NOT_ASSIGNED_LABEL = 'Non assegnato'

DOCUMENT_PERMISSIONS = (
    'checkouts.checkout_document',
    'checkouts.checkout_detail_view',
    'checkouts.checkin_document',
    'checkouts.checkin_document_override',
    'comments.comment_view',
    'comments.comment_create',
    'converter.transformation_edit',
    'converter.transformation_view',
    'converter.transformation_delete',
    'converter.transformation_create',
    'documents.document_version_revert',
    'documents.document_view',
    'documents.document_trash',
    'documents.document_print',
    'documents.document_new_version',
    'documents.document_restore',
    'documents.document_delete',
    'documents.document_edit',
    'documents.document_properties_edit',
    'documents.document_download',
    'mailing.mail_link',
    'mailing.mail_document',
    'metadata.metadata_document_edit',
    'metadata.metadata_document_view',
    'metadata.metadata_document_remove',
)
