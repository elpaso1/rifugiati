# Patch for imagefit

import urlparse

from django.core.urlresolvers import reverse
from django.template import Library

from ..utils import get_appartamento_from_metadata, get_equipe_from_metadata
from threadlocals.threadlocals import get_current_request
from django_countries.fields import CountryField

register = Library()


@register.filter
def resize(value, size, root_name='resize'):
    """
    Generates the url for the resized image prefixing with prefix_path
    return string url
    """
    request = get_current_request()
    return ('https://' if request.is_secure() else 'http://') + request.get_host() + reverse('rifugiati_commons:imagefit_resize', kwargs=dict(
        path_name=root_name, format=size, url=urlparse.unquote(value)))


@register.filter
def media_resize(value, size):
    """
    Generates the url for the resized image prefixing with MEDIA_ROOT
    return string url
    """
    return resize(value, size, 'media_resize')


@register.filter
def static_resize(value, size):
    """
    Generates the url for the resized image prefixing with STATIC_ROOT
    return string url
    """
    return resize(value, size, 'static_resize')


@register.simple_tag
def document_equipe_property(document, prop):
    """Get equipe from document and return the property"""
    try:
        equipe = get_equipe_from_metadata(document)
        return "%s" % getattr(equipe, prop)
    except:
        return 'Nessun equipe'


@register.simple_tag
def document_appartamento_property(document, prop):
    """Get Appartamento from document and return the property"""
    try:
        apartment = get_appartamento_from_metadata(document)
        return "%s" % getattr(apartment, prop)
    except:
        return 'Nessun appartamento'

