
from __future__ import absolute_import, unicode_literals
from django.shortcuts import get_object_or_404
from common.views import (
    SingleObjectDetailView, SingleObjectListView
)
from acls.models import AccessControlList
from documents.permissions import permission_document_view

from django.forms.widgets import Widget

from django.utils.safestring import mark_safe

from tinymce.widgets import TinyMCE

class HTMLWidget(Widget):

    def get_context(self, name, value, attrs=None):
        return {'widget': {
            'name': name,
            'value': value,
        }}

    def render(self, name, value, attrs=None):
        return mark_safe('<div disabled="disabled">%s</div>' % value)



# Create your views here.
class RifugiatiSingleObjectDetailView(SingleObjectDetailView):
    fields = '__all__'

    model = None # To be set in subclass
    object_permission = None # To be set in subclass

    def get_object(self):
        obj = get_object_or_404(self.model, pk=self.kwargs['pk'])

        AccessControlList.objects.check_access(
            permissions=self.object_permission, user=self.request.user,
            obj=obj
        )

        return obj


    def get_document_queryset(self):
        queryset = AccessControlList.objects.filter_by_access(
            permission=permission_document_view, user=self.request.user,
            queryset=self.get_object().get_documents().all()
        )
        return queryset

    def get_diari_queryset(self):
        queryset = []
        try:
            queryset = self.get_object().get_diari().order_by('-id').all()
        except Exception as e:
            pass
        return queryset

    def get_context_data(self, **kwargs):
        """
        Filter documents by tag
        """
        data = super(RifugiatiSingleObjectDetailView, self).get_context_data(**kwargs)
        obj = self.get_object()
        qs = self.get_document_queryset()

        tags = set()
        [[tags.add(_d.label) for _d in d.tags.all()] for d in qs if d.tags.count()]
        sorted(tags)
        if self.request.method == 'GET':
            tag = self.request.GET.get('tag')
        else:
            tag = self.request.POST.get('tag')
        if tag:
            qs = qs.filter(tags__label=tag)
        data.update(
            {
                'document_list': qs,
                'hide_links': True,
                'object': obj,
                'frozen_form': True,
                'tags': tags,
                'filtered_tag': tag,
                'diari_list': self.get_diari_queryset(),
            }
        )
        return data

    def get_form(self):
        """Turn HTML widgets into normal HTML code"""
        form = super(RifugiatiSingleObjectDetailView, self).get_form()
        for fld in form.fields.values():
            if isinstance( fld.widget, TinyMCE):
                fld.widget = HTMLWidget()
        return form



# Create your views here.
class RifugiatiSingleObjectListView(SingleObjectListView):

    def get_queryset(self):
        return self.model.equipe_accessible_objects.all()

