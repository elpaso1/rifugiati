# Create your tests here.
from __future__ import absolute_import
from django.test import TestCase, override_settings
from django.core.management import call_command
from rifugiati_commons.management.commands.metadata_init import METADATA_NAMES
from metadata.models import MetadataType, DocumentType, DocumentTypeMetadataType
from rifugiati_commons.utils import sync_role_group, delete_role_group, get_equipe_from_metadata, get_equipe_from_metadata, get_document_type_name
from django.contrib.auth.models import Group
from permissions.models import Role
from documents.models import Document
from rifugiati_commons.literals import APPARTAMENTO_METADATA_NAME, BENEFICIARIO_METADATA_NAME, EQUIPE_METADATA_NAME

   

@override_settings(OCR_AUTO_OCR=False, 
                    CELERY_EAGER_PROPAGATES_EXCEPTIONS=True,
                    CELERY_ALWAYS_EAGER=True,
                    BROKER_BACKEND='memory')                    
class DocumentHandlerTestCase(TestCase):
    """Test the Document Handlers"""

    @classmethod
    def setUpClass(cls):
        super(DocumentHandlerTestCase, cls).setUpClass()
        call_command('metadata_init')
        # Use the populate command to create some initial documents
        call_command('populate', numero_rifugiati=2, numero_documenti=2, numero_equipe=1, numero_appartamenti=2)


    def test_get_document_equipe(self):
        """Test that equipe can be obtained by document metadata"""
        self.assertGreater(Document.objects.count(), 0)
        for dtn in METADATA_NAMES:
            dtlabel = get_document_type_name(dtn[0])
            docs = Document.objects.filter(document_type__label=dtlabel)
            self.assertGreater(docs.count(), 0)
            for d in docs:
                self.assertIsNotNone(get_equipe_from_metadata(d))
     
   
    def test_acls(self):
        """Check wether ACLs have been set for documents"""
        self.assertGreater(Document.objects.count(), 0)
        for d in Document.objects.filter(document_type__label=get_document_type_name(EQUIPE_METADATA_NAME)):
            self.assertEqual(d.acls.count(), 1)
            # Check that ACL is for equipe members
            self.assertEqual(d.acls.all()[0].role.label, get_equipe_from_metadata(d).metadata_value)
