# Create your tests here.
from __future__ import absolute_import
from django.test import TestCase, override_settings
from django.core.management import call_command
from rifugiati_commons.management.commands.metadata_init import METADATA_NAMES
from metadata.models import MetadataType, DocumentType, DocumentTypeMetadataType
from rifugiati_commons.utils import sync_role_group, delete_role_group, get_equipe_from_metadata, get_equipe_from_metadata, get_document_type_name
from django.contrib.auth.models import Group
from permissions.models import Role
from documents.models import Document
from rifugiati_commons.literals import (CATEGORY_METADATA_LOOKUP_VALUES, CATEGORY_METADATA_NAME, BENEFICIARIO_METADATA_NAME)

# Create your tests here.

@override_settings(OCR_AUTO_OCR=False)
class MetadataInitCommandTestCase(TestCase):
    """Test the metadata init command"""

    @classmethod
    def setUpClass(cls):
        super(MetadataInitCommandTestCase, cls).setUpClass()
        call_command('metadata_init')
    
    def test_metadata_were_created(self):
        """Test wether metadata types are here"""
        for o in METADATA_NAMES:
            mt = MetadataType.objects.get(name=o[0], label=o[0], lookup=u'{{ %s }}' % o[1])
            dt = DocumentType.objects.get(label=get_document_type_name(o[0]))
            self.assertEqual(DocumentTypeMetadataType.objects.filter(document_type=dt, metadata_type=mt, required=True).count(), 1)
        # Check categoria
        self.assertEqual(DocumentTypeMetadataType.objects.filter(document_type=DocumentType.objects.get(label=u'Documento %s' % BENEFICIARIO_METADATA_NAME), metadata_type=MetadataType.objects.get(name=BENEFICIARIO_METADATA_NAME, label=BENEFICIARIO_METADATA_NAME), required=True).count(), 1)


    def test_sync_and_delete_role_group(self):
        """Create role and group"""
        key_value = "[%s-%s]" % ('tst', 1)
        sync_role_group(key_value, "TEST METADATA %s" % key_value)
        self.assertEqual(Role.objects.filter(label__endswith=key_value).count(), 1)
        self.assertEqual(Group.objects.filter(name__endswith=key_value).count(), 1)
        delete_role_group(key_value)
        self.assertEqual(Role.objects.filter(label__endswith=key_value).count(), 0)
        self.assertEqual(Group.objects.filter(name__endswith=key_value).count(), 0)        

    
