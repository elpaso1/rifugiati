# -*- coding: utf-8 -*-
import os
import random
import tempfile

import names
from django.core.management import call_command
from django.core.management.base import BaseCommand
from django_countries.data import COUNTRIES

from appartamenti.models import Appartamento
from beneficiari.models import Beneficiario
from equipe.models import Equipe
from metadata.models import (DocumentMetadata, DocumentType,
                             DocumentTypeMetadataType, MetadataType)
from rifugiati_commons.utils import get_document_type_name

SAMPLE_DOCUMENT_PATH = os.path.join(os.path.dirname(__file__), 'populate_samples', 'title_page.png')


def make_test_document(typename, value, num=1):
    """Create num documents with given documenttype name and metadata value"""
    fd = open(SAMPLE_DOCUMENT_PATH)
    for i in range(0, num):
        d = DocumentType.objects.get(label=get_document_type_name(typename)).new_document(file_object=fd)
        d.label = '%s %s %s.png' % (typename, value, i)
        d.save()
        dm = DocumentMetadata(document=d, metadata_type=MetadataType.objects.get(label=typename))
        dm.value = value
        dm.save()
    fd.close()



class Command(BaseCommand):
    help = 'Create test rifugiati, equipe and appartamenti for the Rifugiati project'


    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument(
            '--numero_rifugiati',
            dest='numero_rifugiati',
            default=10,
            help='Numero rifugiati per appartamento',
        )
        parser.add_argument(
            '--numero_appartamenti',
            dest='numero_appartamenti',
            default=10,
            help='Numero appartamenti per equipe',
        )

        parser.add_argument(
            '--numero_equipe',
            dest='numero_equipe',
            default=5,
            help='Numero equipe',
        )

        parser.add_argument(
            '--numero_documenti',
            dest='numero_documenti',
            default=5,
            help='Numero documenti per ciascun elemento (beneficiario, equipe, appartamento)',
        )
        

    def _attachment(self, typename, instance, num):
        value = instance.metadata_value
        if DocumentMetadata.objects.filter(value=value).count() >= num:
            self.stdout.write(self.style.NOTICE('Skipping attachments for %s %s' % (typename, value)))
            return

        make_test_document(typename, value, num)

    def handle(self, *args, **options):

        call_command('loaddata', 'comuni_italiani.json')

        for i in range(int(options['numero_equipe'])):
            try:
                e = Equipe.objects.get(name='Test Equipe %s' % i)
            except Equipe.DoesNotExist:
                e, c = Equipe.objects.get_or_create(name='Test Equipe %s' % i, region_id=random.randint(1, 3))
                self.stdout.write(self.style.SUCCESS('Equipe %s was successfully created' % e.metadata_value))
            self._attachment('Equipe', e, int(options['numero_documenti']))
            for j in range(int(options['numero_appartamenti'])):
                try:
                    a = Appartamento.objects.get(equipe=e, name='Test Appartamento %s %s' % (e.key_value, j))
                except Appartamento.DoesNotExist:
                    a, c = Appartamento.objects.get_or_create(equipe=e, name='Test Appartamento %s %s' % (e.key_value, j), city_id=random.randint(1, 100))
                    self.stdout.write(self.style.SUCCESS('Appartamento %s was successfully created' % a.metadata_value))
                self._attachment('Appartamento', a, int(options['numero_documenti']))
                for k in range(int(options['numero_rifugiati'])):
                    # Key is the address because the name is random
                    address = 'Forest St. n° %s-%s-%s.' % (i, j, k)
                    try:
                        b = Beneficiario.objects.get(home_address=address)
                    except Beneficiario.DoesNotExist:
                        b, c = Beneficiario.objects.get_or_create(apartment=a, home_address=address, country=COUNTRIES.keys()[random.randint(0, len(COUNTRIES.keys())-1)], equipe=e, name=names.get_first_name(), family_name=names.get_last_name(), in_project=True, birthday='1999-12-31', entry_date='2017-07-26' )
                        self.stdout.write(self.style.SUCCESS('Beneficiario %s was successfully created' % b.metadata_value))
                    self._attachment('Beneficiario', b, int(options['numero_documenti']))

        self.stdout.write(self.style.SUCCESS('All test data for rifugiati were successfully created'))
