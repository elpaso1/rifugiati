from django.core.management.base import BaseCommand

from metadata.models import (DocumentType, DocumentTypeMetadataType,
                             MetadataType)
from sources.models import WebFormSource
from rifugiati_commons.literals import (APPARTAMENTO_METADATA_LOOKUP_FUNCTION_NAME,
                                        APPARTAMENTO_METADATA_NAME,
                                        BENEFICIARIO_METADATA_NAME,
                                        BENEFICIARIO_METADATA_LOOKUP_FUNCTION_NAME,
                                        EQUIPE_METADATA_NAME,
                                        EQUIPE_METADATA_LOOKUP_FUNCTION_NAME,
                                        CATEGORY_METADATA_LOOKUP_VALUES,
                                        CATEGORY_METADATA_NAME,
                                        AVVISO_DI_SCADENZA_METADATA_NAME,
                                        AVVISO_DI_SCADENZA_EMAIL_METADATA_NAME,
                                        )

# TODO: needed?
#from rifugiati_commons.literals import CATEGORY_METADATA_NAME, CATEGORY_METADATA_LOOKUP_FUNCTION_NAME

METADATA_NAMES = (
    (APPARTAMENTO_METADATA_NAME, APPARTAMENTO_METADATA_LOOKUP_FUNCTION_NAME),
    (EQUIPE_METADATA_NAME, EQUIPE_METADATA_LOOKUP_FUNCTION_NAME ),
    (BENEFICIARIO_METADATA_NAME, BENEFICIARIO_METADATA_LOOKUP_FUNCTION_NAME),
)



class Command(BaseCommand):
    help = 'Create metadata types, web upload form and document types for Rifugiati project (%s)' % (', '.join([m[0] for m in METADATA_NAMES]))

    def handle(self, *args, **options):

        # Create metadata and document types
        for o in METADATA_NAMES:
            mt, created = MetadataType.objects.get_or_create(name=o[0], label=o[0], lookup=u'{{ %s }}' % o[1])
            self.stdout.write(self.style.SUCCESS('Initial MetadataType %s for rifugiati was successfully %s' % (o[0], 'created' if created else 'updated')))
            dt, created = DocumentType.objects.get_or_create(label=u'Documento %s' % o[0])
            self.stdout.write(self.style.SUCCESS('Initial DocumentType %s for rifugiati was successfully %s' % (o[0], 'created' if created else 'updated')))
            dtmt, created = DocumentTypeMetadataType.objects.get_or_create(document_type=dt, metadata_type=mt, required=True)
            self.stdout.write(self.style.SUCCESS('Initial DocumentTypeMetadataType %s for rifugiati was successfully %s' % (o[0], 'created' if created else 'updated')))

        # Create categoria
        cat_mt, created = MetadataType.objects.get_or_create(name=CATEGORY_METADATA_NAME, label=CATEGORY_METADATA_NAME, lookup=','.join(CATEGORY_METADATA_LOOKUP_VALUES))
        dtmt, created = DocumentTypeMetadataType.objects.get_or_create(document_type=DocumentType.objects.get(label=u'Documento %s' % BENEFICIARIO_METADATA_NAME), metadata_type=cat_mt, required=True)
        self.stdout.write(self.style.SUCCESS('Initial MetadataType %s for rifugiati was successfully %s' % (CATEGORY_METADATA_NAME, 'created' if created else 'updated')))

        # Create avviso di scadenza
        avv_mt, created = MetadataType.objects.get_or_create(name=AVVISO_DI_SCADENZA_METADATA_NAME, label=AVVISO_DI_SCADENZA_METADATA_NAME, validation='rifugiati_commons.metadata_validators.ItalianDateValidator', parser='rifugiati_commons.metadata_parsers.ItalianDateParser')
        dtmt, created = DocumentTypeMetadataType.objects.get_or_create(document_type=DocumentType.objects.get(label=u'Documento %s' % BENEFICIARIO_METADATA_NAME), metadata_type=avv_mt, required=False)
        self.stdout.write(self.style.SUCCESS('Initial MetadataType %s for rifugiati was successfully %s' % (AVVISO_DI_SCADENZA_METADATA_NAME, 'created' if created else 'updated')))

        # Create email per avviso di scadenza
        avv_mt, created = MetadataType.objects.get_or_create(name=AVVISO_DI_SCADENZA_EMAIL_METADATA_NAME, label=AVVISO_DI_SCADENZA_EMAIL_METADATA_NAME, validation='rifugiati_commons.metadata_validators.EmailValidator')
        dtmt, created = DocumentTypeMetadataType.objects.get_or_create(document_type=DocumentType.objects.get(label=u'Documento %s' % BENEFICIARIO_METADATA_NAME), metadata_type=avv_mt, required=False)
        self.stdout.write(self.style.SUCCESS('Initial MetadataType %s for rifugiati was successfully %s' % (AVVISO_DI_SCADENZA_EMAIL_METADATA_NAME, 'created' if created else 'updated')))


        # Create a web form if there aren't any
        if not WebFormSource.objects.count():
            WebFormSource.objects.create(label='Caricamento documenti')
            self.stdout.write(self.style.SUCCESS('Web form for document upload was successfully created'))
        self.stdout.write(self.style.SUCCESS('All initial metadata for rifugiati were successfully created/updated'))
