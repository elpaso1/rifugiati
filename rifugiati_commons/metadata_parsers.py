from __future__ import unicode_literals

from metadata.parsers import MetadataParser
from dateutil.parser import parserinfo
from dateutil.parser import parse

"""Localized italian date parser for metadata"""


class ItalianDateParser(MetadataParser):
    def execute(self, input_data):
        return parse(input_data, parserinfo=parserinfo(dayfirst=True)).date().isoformat()

