# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import re
from django.db import models
from documents.models import Document
from metadata.models import DocumentType
from .literals import APP_METADATA_MAP
from .utils import get_document_type_name
# Create your models here.


class UserEquipeManager(models.Manager):
    """Returns the only objects that are accessible by the current user"""

    # For testability
    _current_user = None

    def get_queryset(self):
        from threadlocals.threadlocals import get_current_user
        from equipe.models import Equipe
        current_user = self._current_user if self._current_user is not None else get_current_user()
        if current_user is None:
            return super(UserEquipeManager, self).get_queryset()
        equipe_ids = Equipe.get_equipe_ids_for_user(current_user)
        if not equipe_ids:
            return super(UserEquipeManager, self).get_queryset()
        kwargs = {
            self.model.equipe_filter: equipe_ids,
        }
        return super(UserEquipeManager, self).get_queryset().filter(**kwargs)


class RifugiatiAbstractBaseModel(models.Model):
    """Abstract base model for rifugiati classes"""

    objects = models.Manager() # The default manager.

    # The queryset filter to restrict this object,
    # this needs to be overridden in subclasses
    equipe_filter = 'equipe__pk__in'
    # Filter user-equipe-accessible objects
    equipe_accessible_objects = UserEquipeManager()


    @classmethod
    def get_metadata_name(cls):
        """Return Metadata name (from literals)"""
        return APP_METADATA_MAP[cls._meta.app_label]

    @classmethod
    def get_document_type(cls):
        """Get document type for this object"""
        return DocumentType.objects.get(label=get_document_type_name(cls.get_metadata_name()))


    @classmethod
    def mod_prefix(cls):
        return cls._meta.app_label[:3]

    @classmethod
    def get_from_metadata(cls, metadata_value):
        """Return object from metadata value (or raise DoesNotExist)"""
        # Get the key from the md value
        try:
            mod, pk = re.findall(r'\[(.{3})-(\d+)\]$', metadata_value)[0]
        except IndexError:
            raise cls.DoesNotExist
        if mod != cls.mod_prefix():
            raise cls.DoesNotExist
        return cls.objects.get(pk=int(pk))

    @property
    def key_value(self):
        return "[%s-%s]" % (self.mod_prefix(), self.pk)

    @property
    def metadata_value(self):
        return ("%s %s" % (self, self.key_value)).replace(',', ';')

    def get_documents(self):
        """Get documents associated with this object (through metadata type and value)"""
        return self.get_document_type().documents.filter(metadata__value__endswith=self.key_value).order_by('date_added')


    def delete(self):
        """Delete all related documents"""
        self.get_documents().delete()
        super(RifugiatiAbstractBaseModel, self).delete()

    class Meta:
        abstract = True

