#!/bin/bash
# Configure the application

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $DIR/settings.sh

echo "Configuring application ...."


###########################################################################
# Change to unprivileged

sudo -H -u $DEST_USER /bin/bash - << EOF
cd

cd rifugiati

if [ ! -d media ]; then
    mkdir media
fi

if [ ! -d tmp ]; then
    mkdir tmp
    chmod 777 tmp
fi

if [ ! -d media/static ]; then
    mkdir media/static
fi


# Create storage directory
if [ ! -d media/document_storage/ ]; then
        mkdir media/document_storage/
fi

cp /vagrant/vagrant/overrides/dj .
sed -i -e "s#RIFUGIATI_INSTALL_DIR#${RIFUGIATI_INSTALL_DIR}#" dj
chmod +x dj

cp /vagrant/vagrant/overrides/settings_local.py .
sed -i -e "s#RIFUGIATI_INSTALL_DIR#${RIFUGIATI_INSTALL_DIR}#" settings_local.py


# App setup

./dj migrate
./dj collectstatic --noinput
./dj loaddata comuni_italiani.json
./dj metadata_init

# Debug only
if [ "${DEBUG}" = '1' ]; then
    ./dj createautoadmin
    # Create test users and documents
    ./dj populate --numero_rifugiati 2 --numero_appartamenti 2 --numero_equipe 2 --numero_documenti 2
else
    echo "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'admin')" | ./dj shell
fi

./dj comuni_make_labels

EOF


# Fix ownership
chown -R $DEST_USER:www-data $RIFUGIATI_INSTALL_DIR
chmod -R g+rw $RIFUGIATI_INSTALL_DIR

# Restart services
service uwsgi restart
service nginx restart

