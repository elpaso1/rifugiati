#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "Reading settings ..."

# The unprivileged user on the system, the app will be installed
# inside /home/DEST_USER/rifugiati
export DEST_USER=ubuntu
export DEBUG=0
export RIFUGIATI_INSTALL_DIR=/home/${DEST_USER}/rifugiati
