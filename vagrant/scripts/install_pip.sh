#!/bin/bash
# Configure the application

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $DIR/settings.sh

echo "Installing pip and packages ..."


# Update to latest version of pip
pip install -U pip

# Virtualenv
pip install virtualenv

###########################################################################
# Change to unprivileged

sudo -H -u $DEST_USER /bin/bash - << EOF
cd
git clone https://gitlab.com/elpaso1/rifugiati.git
virtualenv --system-site-packages rifugiati
. /home/${DEST_USER}/rifugiati/bin/activate
# Install rifugiati
pip install -r rifugiati/requirements.txt

# Install Python clients for PostgreSQL, REDIS, librabbitmq and uWSGI
pip install psycopg2 redis librabbitmq

# Install Django storages alternate file storages
pip install django-storages boto boto3

# Install Mayan EDMS, latest production release
pip install git+https://gitlab.com/elpaso1/mayan-edms.git@master

EOF