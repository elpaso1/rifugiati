#!/bin/bash
# Configure the application

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# This is only for Vagrant:
cp /vagrant/vagrant/scripts/*.sh /tmp

. $DIR/settings.sh

. $DIR/install_debs.sh
. $DIR/install_pip.sh
. $DIR/config_services.sh
. $DIR/config_app.sh

#. $DIR/clean.sh