#!/bin/bash
# Configure the application

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $DIR/settings.sh

echo "Setting up services ..."

# Create a pg user

sudo -u postgres bash -c "psql -c \"CREATE USER rifugiati WITH PASSWORD 'rifugiati';\""

# Create the DB
sudo -u postgres createdb rifugiati

# Setup uWSGI
cp /vagrant/vagrant/etc/uwsgi/rifugiati.ini $RIFUGIATI_INSTALL_DIR
sed -i -e "s#RIFUGIATI_INSTALL_DIR#${RIFUGIATI_INSTALL_DIR}#" $RIFUGIATI_INSTALL_DIR/rifugiati.ini
if [ ! -e /etc/uwsgi/apps-enabled/rifugiati.ini ]; then
        ln -s $RIFUGIATI_INSTALL_DIR/rifugiati.ini /etc/uwsgi/apps-enabled/rifugiati.ini
fi

# Setup NGINX
cp /vagrant/vagrant/etc/nginx/rifugiati.template /etc/nginx/sites-available/rifugiati.template
sed -i -e "s#RIFUGIATI_INSTALL_DIR#${RIFUGIATI_INSTALL_DIR}#" /etc/nginx/sites-available/rifugiati.template
if [ ! -e /etc/nginx/sites-enabled/rifugiati.template ]; then
        ln -s /etc/nginx/sites-available/rifugiati.template /etc/nginx/sites-enabled/
fi

if [ -e /etc/nginx/sites-enabled/default ]; then
        rm /etc/nginx/sites-enabled/default
fi


cp /vagrant/vagrant/etc/systemd/system/celerybeat.service /lib/systemd/system/celerybeat.service
sed -i -e "s#RIFUGIATI_INSTALL_DIR#${RIFUGIATI_INSTALL_DIR}#" /lib/systemd/system/celerybeat.service
cp /vagrant/vagrant/etc/systemd/system/celeryd.service /lib/systemd/system/celeryd.service
sed -i -e "s#RIFUGIATI_INSTALL_DIR#${RIFUGIATI_INSTALL_DIR}#" /lib/systemd/system/celeryd.service
chmod +x /lib/systemd/system/celeryd.service
chmod +x /lib/systemd/system/celerybeat.service
systemctl enable celeryd
systemctl enable celerybeat
systemctl start celeryd
systemctl start celerybeat

#cp /vagrant/vagrant/etc/default/celeryd /etc/default/
#sed -i -e "s#RIFUGIATI_INSTALL_DIR#${RIFUGIATI_INSTALL_DIR}#" /etc/default/celeryd
#cp /vagrant/vagrant/etc/init.d/celeryd /etc/init.d/
#cp /vagrant/vagrant/etc/init.d/celerybeat /etc/init.d/
#sudo chmod 755 /etc/init.d/celeryd
#sudo chown root:root /etc/init.d/celeryd
#sudo chmod 755 /etc/init.d/celerybeat
#sudo chown root:root /etc/init.d/celerybeat

# Enable celery
#update-rc.d celeryd defaults
#update-rc.d celerybeat defaults

# Setup supervisor
#cp /vagrant/vagrant/etc/supervisor/beat.conf /etc/supervisor/conf.d
#cp /vagrant/vagrant/etc/supervisor/nginx.conf /etc/supervisor/conf.d
#cp /vagrant/vagrant/etc/supervisor/uwsgi.conf /etc/supervisor/conf.d
#cp /vagrant/vagrant/etc/supervisor/redis.conf /etc/supervisor/conf.d
#cp /vagrant/vagrant/etc/supervisor/workers.conf /etc/supervisor/conf.d
#sed -i -e "s#RIFUGIATI_INSTALL_DIR#${RIFUGIATI_INSTALL_DIR}#" /etc/supervisor/conf.d/uwsgi.conf
#sed -i -e "s#RIFUGIATI_INSTALL_DIR#${RIFUGIATI_INSTALL_DIR}#" /etc/supervisor/conf.d/nginx.conf
#sed -i -e "s#RIFUGIATI_INSTALL_DIR#${RIFUGIATI_INSTALL_DIR}#" /etc/supervisor/conf.d/beat.conf
#sed -i -e "s#RIFUGIATI_INSTALL_DIR#${RIFUGIATI_INSTALL_DIR}#" /etc/supervisor/conf.d/redis.conf
#sed -i -e "s#RIFUGIATI_INSTALL_DIR#${RIFUGIATI_INSTALL_DIR}#" /etc/supervisor/conf.d/workers.conf


# Create the directory for the logs
#if [ ! -e /var/log/mayan ]; then
#        mkdir /var/log/mayan
#        chown -R www-data:www-data /var/log/mayan
#fi

