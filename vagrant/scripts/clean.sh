#!/bin/bash
# Configure the application

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $DIR/settings.sh

echo "Cleaning ..."

# Clean
apt-get clean autoclean
apt-get autoremove --purge -y
rm -rf /var/lib/apt/lists/*
rm -f /var/cache/apt/archives/*.deb
