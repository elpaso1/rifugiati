#!/bin/bash
# Configure the application

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
. $DIR/settings.sh

echo "Installing debs ..."


export DEBIAN_FRONTEND=noninteractive

apt-get update

# Update system
apt-get -o Dpkg::Options::="--force-confnew" dist-upgrade -y

apt-get install -y --no-install-recommends \
        curl \
        git \
        gcc \
        gettext-base \
        ghostscript \
        gpgv \
        graphviz \
        libjpeg-dev \
        libmagic1 \
        libmysqlclient-dev \
        libpng-dev \
        libpq-dev \
        libreoffice \
        libtiff-dev \
        locales \
        nginx \
        netcat-openbsd \
        poppler-utils \
        python-dev \
        python-pip \
        python-setuptools \
        python-wheel \
        redis-server \
        tesseract-ocr \
        build-essential \
        libffi-dev \
        postgresql \
        tesseract-ocr-ita \
        uwsgi \
        uwsgi-plugin-python \
        software-properties-common \
        apticron \


# Switch to UTF locale
echo "LC_ALL=\"it_IT.UTF-8\"" >> /etc/default/locale
locale-gen it_IT.UTF-8
update-locale LANG=it_IT.UTF-8
export LC_ALL=it_IT.UTF-8


# Fix error Error: failed to load /usr/share/tesseract-ocr/tessdata/ita.special-words
touch /usr/share/tesseract-ocr/tessdata/ita.special-words

# Install let'encrypt bot
# See: https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-16-04
apt-get update
apt-get install python-certbot-nginx
# vi /etc/nginx/sites-enabled/rifugiati.template
#sudo certbot --nginx -d covmigranti.org -d www.covmigranti.org
# crontab -e
# 5 2 * * * /usr/bin/certbot renew --quiet
# Restart nginx and cron

# Change notification emails:
# vim /etc/apticron/apticron.conf