from django.conf.urls import url
from appartamenti.views import *

urlpatterns = [
    url(r'^$', AppartamentoList.as_view(), name='appartamento_list'),
    url(r'^(?P<pk>\d+)/$', AppartamentoDetail.as_view(), name='appartamento_detail'),
    url(r'^create/$', AppartamentoCreate.as_view(), name='appartamento_create'),
    url(r'^(?P<pk>\d+)/delete/$', AppartamentoDelete.as_view(), name='appartamento_delete'),
    url(r'^(?P<pk>\d+)/edit/$', AppartamentoEdit.as_view(), name='appartamento_edit'),
]

api_urls = []