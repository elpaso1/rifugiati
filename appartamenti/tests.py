# -*- coding: utf-8 -*-
# Create your tests here.
from __future__ import absolute_import
from django.test import TestCase, override_settings
from django.core.management import call_command
from appartamenti.models import Appartamento
from appartamenti.literals import APPARTAMENTO_PERMISSIONS
from django.contrib.auth.models import Group, User
from rifugiati_commons.utils import get_document_type_name



@override_settings(OCR_AUTO_OCR=False, 
                    CELERY_EAGER_PROPAGATES_EXCEPTIONS=True,
                    CELERY_ALWAYS_EAGER=True,
                    BROKER_BACKEND='memory')                    
class AppartamentiHandlerTestCase(TestCase):
    """Test the Document Handlers"""

    @classmethod
    def setUpClass(cls):
        super(AppartamentiHandlerTestCase, cls).setUpClass()
        call_command('metadata_init')
        # Use the populate command to create some initial documents
        call_command('populate', numero_rifugiati=2, numero_documenti=2, numero_equipe=2, numero_appartamenti=2)
    
   
    def test_equipe_accessible_manager(self):
        """Test custom manager"""
        Appartamento.equipe_accessible_objects._current_user = None
        user = User.objects.create(username='User 999', password='User 999')
        Appartamento.equipe_accessible_objects._current_user = user
        self.assertEqual(Appartamento.equipe_accessible_objects.all().count(), 4)
        user.groups.add(Group.objects.get(name__contains='Test Equipe 0'))
        self.assertEqual(Appartamento.equipe_accessible_objects.all().count(), 2)
        self.assertEqual(list(Appartamento.equipe_accessible_objects.all()), list(Appartamento.objects.filter(equipe__name__contains='Test Equipe 0')))
        # Both equipes
        user.groups.add(Group.objects.get(name__contains='Test Equipe 1'))
        self.assertEqual(Appartamento.equipe_accessible_objects.all().count(), 4)
        self.assertEqual(list(Appartamento.equipe_accessible_objects.filter(equipe__name__contains='Test Equipe 0')), list(Appartamento.objects.filter(equipe__name__contains='Test Equipe 0')))
        self.assertEqual(list(Appartamento.equipe_accessible_objects.filter(equipe__name__contains='Test Equipe 1')), list(Appartamento.objects.filter(equipe__name__contains='Test Equipe 1')))
        user.delete()
        Appartamento.equipe_accessible_objects._current_user = None
   

    def test_acls(self):
        """Check wether ACLs have been set for appartamento"""
        self.assertGreater(Appartamento.objects.count(), 0)
        for d in Appartamento.objects.all():
            self.assertEqual(d.acls.count(), 1)
            # Check that ACL is for Appartamento members
            self.assertEqual(d.acls.all()[0].role.label, d.equipe.metadata_value)

    def test_get_documents(self):
        """Test retrieve documents and document types"""
        a = Appartamento.objects.all()[0]
        self.assertEqual(a.get_document_type().label, get_document_type_name(a.get_metadata_name()))
        # Check that metadata matches
        self.assertTrue(a.get_documents().count())
        for d in a.get_documents():
            self.assertEqual(d.metadata_value_of.Appartamento, a.metadata_value)
        
