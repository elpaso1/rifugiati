from __future__ import absolute_import, unicode_literals

from django.utils.translation import ugettext_lazy as _

from permissions import PermissionNamespace

namespace = PermissionNamespace('appartamenti', _('Appartamenti'))

permission_appartamento_create = namespace.add_permission(
    name='appartamento_create', label=_('Crea appartamenti')
)
permission_appartamento_delete = namespace.add_permission(
    name='appartamento_delete', label=_('Elimina appartamenti')
)
permission_appartamento_edit = namespace.add_permission(
    name='appartamento_edit', label=_('Modifica appartamenti')
)
permission_appartamento_view = namespace.add_permission(
    name='appartamento_view', label=_('Visualizza appartamenti')
)
