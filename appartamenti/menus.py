from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _

from navigation import Menu

menu_appartamenti = Menu(
    icon='fa fa-home', label=_('Appartamenti'), name='appartamenti menu'
)
