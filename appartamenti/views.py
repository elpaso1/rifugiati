# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.urlresolvers import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import ungettext
from django.views.generic import DetailView, ListView, TemplateView

from appartamenti.models import Appartamento
from common.views import (SingleObjectCreateView, SingleObjectDeleteView,
                          SingleObjectDetailView, SingleObjectEditView )
from rifugiati_commons.views import RifugiatiSingleObjectDetailView, RifugiatiSingleObjectListView

from .forms import AppartamentoForm
from .permissions import (permission_appartamento_create,
                          permission_appartamento_delete,
                          permission_appartamento_edit,
                          permission_appartamento_view)


class AppartamentoList(RifugiatiSingleObjectListView):
    model = Appartamento


class AppartamentoDetail(RifugiatiSingleObjectDetailView):
    model = Appartamento
    object_permission = permission_appartamento_view
    template_name = 'appartamenti/appartamento_detail.html'

    def get_extra_context(self):
        return {
            'beneficiari_in_project': self.get_object().beneficiario_set.filter(in_project=True),
            'beneficiari_out_project': self.get_object().beneficiario_set.filter(in_project=False),
        }


class AppartamentoCreate(SingleObjectCreateView):
    model = Appartamento
    view_permission = permission_appartamento_create
    form_class = AppartamentoForm
    post_action_redirect = reverse_lazy('appartamenti:appartamento_list')

    def get_extra_context(self):
        return {
            'title': _('Crea Appartamento'),
        }


class AppartamentoEdit(SingleObjectEditView):
    #fields = ('name', 'description', 'region')
    fields = '__all__'
    model = Appartamento
    object_permission = permission_appartamento_edit
    post_action_redirect = reverse_lazy('appartamenti:appartamento_list')

    def get_extra_context(self):
        return {
            'object': self.get_object(),
            'title': _('Modifica Appartamento: %s') % self.get_object(),
        }


class AppartamentoDelete(SingleObjectDeleteView):
    model = Appartamento
    object_permission = permission_appartamento_delete
    post_action_redirect = reverse_lazy('appartamenti:appartamento_list')

    def get_extra_context(self):
        return {
            'object': self.get_object(),
            'title': _('Elimina Appartamento: %s?') % self.get_object(),
            'relations': self.get_object().beneficiario_set.all(),
        }
