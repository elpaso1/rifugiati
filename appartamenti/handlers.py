from __future__ import absolute_import, unicode_literals

import logging

from cabinets.models import Cabinet
from equipe.literals import *
from metadata.models import MetadataType
from rifugiati_commons.utils import add_instance_acl_to_equipe
from rifugiati_commons.literals import APPARTAMENTO_METADATA_NAME

from . import permissions as permissions_module
from .literals import APPARTAMENTO_PERMISSIONS

logger = logging.getLogger(__name__)

def on_appartamento_save_handler(sender, **kwargs):
    """Takes care of creating cabinets"""
    appartamento = kwargs['instance']
    logger.debug('instance: %s', appartamento)
    # First check and create a top level equipe cabinet
    try:
        equipe_cabinet = Cabinet.objects.get(label__endswith=appartamento.equipe.key_value)
    except Cabinet.DoesNotExist:
        equipe_cabinet = Cabinet(label=appartamento.equipe.metadata_value)
        equipe_cabinet.save()

    # Check and create a cabinet for the appartamenti, parent is the equipe
    try:
        cabinet = Cabinet.objects.get(label__endswith=appartamento.key_value)
        # Ensure the name matches
        if cabinet.label != appartamento.metadata_value:
            cabinet.label = appartamento.metadata_value
            cabinet.save()
    except Cabinet.DoesNotExist:
        cabinet = Cabinet(label=appartamento.metadata_value, parent=equipe_cabinet)
        cabinet.save()

    # Fix all metadata for documents
    try:
        md = MetadataType.objects.get(name=APPARTAMENTO_METADATA_NAME)
        for dmd in md.documentmetadata_set.filter(value__endswith=appartamento.key_value):
            if dmd.value != appartamento.metadata_value:
                dmd.value = appartamento.metadata_value
                dmd.save()
    except MetadataType.DoesNotExist:
        pass

    # Add permissions
    add_instance_acl_to_equipe(appartamento, APPARTAMENTO_PERMISSIONS, permissions_module, 'permission_appartamento', appartamento.equipe)



def on_appartamento_delete_handler(sender, **kwargs):
    """Takes care of deleting cabinets"""
    appartamento = kwargs['instance']
    logger.debug('instance: %s', appartamento)
    # First check and create a top level equipe cabinet
    try:
        equipe_cabinet = Cabinet.objects.get(label__endswith=appartamento.key_value)
        equipe_cabinet.delete()
    except Cabinet.DoesNotExist:
        pass        