from __future__ import absolute_import, unicode_literals

import copy

from django.utils.translation import ugettext_lazy as _

from acls.links import link_acl_list
from navigation import Link

from .permissions import (
    permission_appartamento_create,
    permission_appartamento_delete, permission_appartamento_edit,
    permission_appartamento_view
)

# appartamento links

link_custom_acl_list = copy.copy(link_acl_list)


link_appartamento_create = Link(
    icon='fa fa-plus', permissions=(permission_appartamento_create,),
    text=_('Crea appartamento'), view='appartamenti:appartamento_create'
)
link_appartamento_delete = Link(
    permissions=(permission_appartamento_delete,), tags='dangerous',
    text=_('Elimina'), view='appartamenti:appartamento_delete', args='object.pk'
)
link_appartamento_edit = Link(
    permissions=(permission_appartamento_edit,), text=_('Modifica'),
    view='appartamenti:appartamento_edit', args='object.pk'
)
link_appartamento_list = Link(
    icon='fa fa-columns', text=_('All'), view='appartamenti:appartamento_list'
)

link_appartamento_view = Link(
    permissions=(permission_appartamento_view,), text=_('Details'),
    view='appartamenti:appartamento_detail', args='object.pk'
)
