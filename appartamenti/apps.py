# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.apps import apps
from django.core.urlresolvers import reverse_lazy
from django.db.models.signals import post_save, pre_delete
from django.utils.translation import ugettext_lazy as _

from acls import ModelPermission
from acls.permissions import permission_acl_edit, permission_acl_view
from common import (MayanAppConfig, menu_facet, menu_main, menu_multi_item,
                    menu_object, menu_secondary, menu_sidebar)
from metadata import MetadataLookup
from rest_api.classes import APIEndPoint

from .links import (link_custom_acl_list, link_appartamento_create,
                    link_appartamento_delete, link_appartamento_edit, link_appartamento_list,
                    link_appartamento_view)
from .menus import menu_appartamenti
from .permissions import (permission_appartamento_delete, permission_appartamento_edit,
                          permission_appartamento_view)

from rifugiati_commons.literals import APPARTAMENTO_METADATA_LOOKUP_FUNCTION_NAME

# Mayan < 2.3
try:
    from commons import menu_front_page
except:
    from common.classes import DashboardWidget


class AppartamentiApp(MayanAppConfig):
    name = 'appartamenti'
    test = True
    verbose_name = _('Appartamenti')

    def ready(self):
        super(AppartamentiApp, self).ready()

        appartamenti = self.get_model('appartamento')

        APIEndPoint(app=self, version_string='1')

        ModelPermission.register(
            model=appartamenti, permissions=(
                permission_acl_edit, permission_acl_view,
                permission_appartamento_delete, permission_appartamento_edit,
                permission_appartamento_view
            )
        )

        
        menu_appartamenti.bind_links(
            links=(
                link_appartamento_list, link_appartamento_create
            )
        )

        menu_main.bind_links(links=(menu_appartamenti,), position=98)

        menu_object.bind_links(
            links=(
                link_appartamento_view, link_appartamento_edit,
                link_custom_acl_list, link_appartamento_delete
            ), sources=(appartamenti,)
        )

        # Mayan < 2,3
        try:
            menu_front_page.bind_links(
                links=( link_appartamento_list, )
            )
        except:
            DashboardWidget(
                icon='fa fa-home', queryset=self.get_model('appartamento').objects.all(),
                label=_('Appartamenti'),
                link=reverse_lazy(
                    'appartamenti:appartamento_list'                    
                )
            )

        # Custom handlers for appartamenti sync
        from .handlers import on_appartamento_save_handler, on_appartamento_delete_handler
        post_save.connect(
            receiver=on_appartamento_save_handler,
            sender=self.get_model('appartamento')
        )
        pre_delete.connect(
            receiver=on_appartamento_delete_handler,
            sender=self.get_model('appartamento')
        )

        # Get all appartamenti
        def get_appartamenti(*args, **kwargs ):
            # If user is in an equipe role, filter by it
            return','.join([e.metadata_value for e in self.get_model('appartamento').equipe_accessible_objects.all()])


        MetadataLookup(
            description=_('Appartamenti'), name=APPARTAMENTO_METADATA_LOOKUP_FUNCTION_NAME,
            value=get_appartamenti
        )

        from actionlogger.models import ActionRegistry
        ActionRegistry.register(self.get_model('appartamento'))
