# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import re
from django.db import models
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext, ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible

from comuni_italiani.models import Comune
from equipe.models import Equipe
from rifugiati_commons.models import RifugiatiAbstractBaseModel
from rifugiati_commons.literals import *


@python_2_unicode_compatible
class Appartamento(RifugiatiAbstractBaseModel):
    """Model definition for Appartamento."""

    @classmethod
    def get_appartamento_for_user(cls, user):
        """Return appartamento filtered by user group, default to all"""
        regexp = re.compile(r'\[.{3}-(\d+)\]')
        try:
            equipe_keys = [int(regexp.findall(g.name)[0]) for g in user.groups.filter(name__contains='[%s-' % Equipe.mod_prefix())]
        except KeyError:
            return cls.objects.all()
        return cls.objects.filter(equipe__pk__in=equipe_keys)


    APARTMENT_NOT_ASSIGNED=APARTMENT_NOT_ASSIGNED_LABEL

    name = models.CharField(verbose_name=_('Nome'), max_length=64, unique=True, db_index=True)
    equipe = models.ForeignKey(Equipe, db_index=True)
    city = models.ForeignKey(Comune, verbose_name=_('Comune'))
    street = models.CharField(verbose_name=_('Via'), max_length=128)
    number = models.CharField(verbose_name=_('N° civico/interno'), max_length=64)
    houselord = models.CharField(verbose_name=_('Proprietario'), max_length=128, blank=True, null=True)
    notes = models.TextField(verbose_name=_('Note'), blank=True, null=True)

    class Meta:
        """Meta definition for Appartamento."""
        verbose_name = _('Appartamento')
        verbose_name_plural = _('Appartamenti')


    def __str__(self):              # __unicode__ on Python 2
        return "%s/%s" % (self.equipe.name, self.name)

    def __unicode__(self):
        return str(self)

    def get_absolute_url(self):
        return reverse('appartamenti:appartamento_detail', args=(self.pk,))

    @property
    def address(self):
        return "%s %s %s" % (self.street, self.number, self.city)

