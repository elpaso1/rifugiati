# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from equipe.models import Equipe
from .models import Appartamento

class AppartamentoForm(forms.ModelForm):

    class Meta:
        model = Appartamento
        fields = '__all__'
        
    def __init__(self,*args,**kwargs):
        super (AppartamentoForm,self ).__init__(*args,**kwargs) # populates the post
        self.fields['equipe'].queryset = Equipe.equipe_accessible_objects.all()

    def clean(self):
        """Checks:
            - apartment and equipe relationship
        """    
        cleaned_data = super(AppartamentoForm, self).clean()
        equipe = cleaned_data.get("equipe")

        if equipe:
            if equipe not in Equipe.equipe_accessible_objects.all():
                raise forms.ValidationError(
                    "Non hai accesso all'equipe selezionato! Correggilo in modo che corrisponda al tuo equipe di appartenenza."
                )
